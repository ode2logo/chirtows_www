# ChirtoWS / www #

Este proyecto tendrá como objetivo desarrollar la interfaz de usuario del proyecto ChirtoWS.

### Detalles ###

* Versión: a determinar.
* [Sitio oficial de Chirto](http://chirto.com.ar)

### Qué necesitás ###

* Mozilla Firefox actualizado. Google Chrome opcional.
* Conocimientos generales de: JS, JQuery, HTML, CSS.
* Editor de textos avanzado: Notepad++ / Atom / Sublime Text . El que mas os guste.
* Ocupar las herramientas de desarrolladores de Firefox o Chrome.
* [Git](https://git-scm.com/downloads) bash o herramienta similar para comunicarse a este repo de BitB.

### Instalación básica de este repo ###

* Con git instalado en el sistema.
* Inicio>Ejecutar>CMD ó Linux>Terminal.
* cd C:\ruta\de\trabajo ó /path/to/work .
* git clone https://bitbucket.org/ode2logo/chirtows_www.git

### Modo de realizar pruebas ###

* wsbat.htm > Abrir con > Mozilla.
* En Mozilla: Options > Developer > Web Console.
* Escribir: redrawMazo(60); ó redrawMazo(45,true);

### Arquitectura básica de la interfaz ###

* El archivo principal: wsbat.htm
Para empezar la descripción, la interfaz presenta un markup html+css (en las últimas lineas de código).
* Tras la carga del archivo, se disparan ciertas funciones iniciales, creadoras de más objetos DOM y algunos eventos para diseño responsivo, como así también el intento de realizar una conexion WS (que por lo pronto no nos importa).
* De modo asincrónico, es necesario que la UI (user-interface) sea capaz de enviar información, recibirla y sobre todo interpretarla.
* En lo que refiere a interpretación, es necesario que la ui realice las animaciones de lo que va sucediendo dentro de la mesa.
* Para el desarrollo de esta ui, será necesario simular el ingreso de la información proporcionada por el servidor, y que los eventos se desarrollen en el orden que son llamados.

### Tutoriales y demos ###
* Consultar archivo [TUTORIALES.md](https:/bitbucket.org/ode2logo/chirtows_www/TUTORIALES.md).

### ToDo y otras informaciones ###
https://trello.com/invite/b/vwWpdUI5/b65b2ba1650c969eea5ebb181716022d/chirtows-www
