# Tutoriales #
### TAREA 0: familiarización con wsbat.htm ###
* En lo que respecta a diseño/CSS, todo puede mejorarse, pero no será una prioridad por el momento.
* En lo que respecta a HTML, el wsbat presenta una estructura jerárquica anidada. Es importante tenerlo presente para usar aropiadamente los selectores JQ.
* b0,b1,b2... son los lugares de las bancas. b0 es el poke activo.
* Al principio se declaran algunas variables globales (una muy abarcativa es <tabla>).
* Esta variable contiene (o deberia contener) toda la información dibujable de la mesa.
* El archivo utiliza funciones "dibujadoras" para describir inmediatamente una situación específica de la mesa.
Sugiero que revisen la función-evento <$(document).ready>. En dicha función hay varias lineas quoteadas, puden descomentarlas para ir probando ciertas acciones.
* Las funciones dibujadoras en muchas ocasiones se basan en argumentos transitorios, mientras que la información de la <tabla> se actualiza independientemente. Es decir (por ejemplo): " En el momento que se realiza la acción redrawMazo(60), se dibuja un mazo con 60 cartas, pero la información de <tabla.cartas[0].m> no se altera... entonces si se recibe una orden de robar 7 cartas, se dibujan intervalos de <redrawMazo(59);redrawMazo(58);redrawMazo(57);...> mientras que  <tabla.cartas[0].m> tendría su valor respectivo (53) inmediatamente. <tabla> siempre es un respaldo de lo que deberia ser la mesa, no de lo que está dibujado. "
* Información que pueden omitir pero les puede interesar:
    - La interfaz tiene que estar preparada tanto para pantallas touch, como para mouse. Sin embargo para la mayoria de las tareas no tendrán que pensar en este asunto.
    - Es posible que en un futuro las mesas tengan más de 2 jugadores (3,4,10... quien sabe), pero siempre se mostrará la mesa enfrentada a 1 rival (pudiendo cambiarse según la voluntad del participante y la característica de la mesa).
* Código a testear: (siempre en consola del navegador)
    - redrawMazo(60);
    - redrawMazo(15,true);
    - makePoke([{img:11,hp:12,dano:9,nombre:"Charizard"},{img:31},{img:53},{img:5},{img:5},{img:5},{img:5},{img:5}],"b0op");
    - makePoke([{img:15,hp:10,dano:3,nombre:"Machamp"},{img:41},{img:59},{img:6},{img:7},{img:6},{img:6}],"b0du");
    - redrawDiscard([14,66,2,1,77,14,66,2,1,77,14,66,2,1,77,14,66,2,1,77,45,35,25,100,78,23,44,33,11],true);
    En la función <$(document).ready> hay mas ejemplos quoteados.

### TAREA 0.1 familiarización con las entradas de wsbat y la cola de animaciones ###
* Cuando wsbat recibe información, llama a la función <interpretarDatos> pasándole por parámetro un txt que luego es convertido a un JSON.
* Dependiendo de esa información, se define si es algún tipo de interacción que se delega al <animCtl> (AnimController) o se realiza otra cosa. El <animCtl> ordena las animaciones.. hay algunas configuraciones en variables globales, pero por lo pronto se hacen 1 a la vez.
* La mayoria de las animaciones llaman a la función <makeAnimaCarta>, ésta función dibuja el movimiento de la carta y cuando finaliza ejecuta un Callback (parámetro <cb>).
* Código a testear:
    - tabla.cartas[0]={m:60,h:[1,2,32,4,56],d:[77,77],p:[0,0,0,0,0,0],t:[],s:[],b0:[{img:14}],b1:[],b2:[],b3:[],b4:[],b5:[]};
      tabla.cartas[1]={m:45,h:[0,0,0],d:[5,5,5],p:[0,0,0],t:[],s:[],b0:[{img:14}],b1:[],b2:[],b3:[],b4:[],b5:[]};
      animCtl.evaluate({key:'tabla'});
    - animCtl.evaluate({tipo:'evento',lado:0,key:'baraja',desc:''});
    - animCtl.evaluate({tipo:'evento',lado:1,key:'robaCartas',desc:[0,0,0]});
