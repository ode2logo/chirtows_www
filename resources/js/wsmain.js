var defaultHost = location.hostname || "ws.chirto.com.ar";
var screen_w = 0;
var screen_h = 0;
var screen_ver = false;
var ws_status = 0;
var ldu = 0;
var lop = 1;
var team = 0;
var hs=location.hash.substr(1).split('|');
var mesa = hs[0];
var cuenta=hs[1];
var str_login = "login "+hs[1]+" "+hs[2];
var str_join = 'join mesa ' + mesa;
var str_refresh = 'main refresh';
var animDelay = 100;
var animSmLado = false;
var animSmPos = false;
var animCtl;
var nextAnim = Date.now();
var lastCartaUsada = null;
var lastclick=Date.now();
var clickdown = 0;
var lastenvio=Date.now();
var lastrecep=Date.now();
var kbfocus = null;
var viendo="";
var rutaimg = "http://img.chirto.com.ar/img/cards2/";
var arr_recards=arr_cards.map(v=>{return {img:v[0],tipo:v[1],subtipo:v[2],nombre:v[3],hp:v[4],color:v[5],maxcopias:(v[0]<7?15:4),precio:v[13],sets:v[12],evolvesfrom:v[17]}});
// var rutaimg="";
var vidablanca = "<img src=resources/images/vidablanca.gif width=15%/>";
var vidanegra = "<img src=resources/images/vidanegra.gif width=15%/>";
var img_estados = {};
	img_estados['c']="<img src=resources/images/confundido.gif width=15%/>";
	img_estados['d']="<img src=resources/images/dormido.gif width=15%/>";
	img_estados['p']="<img src=resources/images/paralizado.gif width=15%/>";
	img_estados['v']="<img src=resources/images/envenenado.gif width=15%/>";
	img_estados['q']="<img src=resources/images/quemado.gif width=15%/>";
	img_estados['i']="<img src=resources/images/congelado.gif width=15%/>";
	img_estados['u']="<img src=resources/images/puno.gif width=15%/>";
	img_estados['f']="<img src=resources/images/escudo.gif width=15%/>";
var let_estados = {'confundido':'c','dormido':'d','paralizado':'p','envenenado':'v','quemado':'q','congelado':'i'};
var nivel_categorias = {g:15,f:35,e:85,d:175,c:300,b:500,a:750,s:999999999};

var defProv=[null,'a','p','e','h','f','l','dd'];
var nidselect = 0;
var sis = [];
var sit;
//doms
var divchatlogs;
var divEMcartas;
var	divEMstat;
var	divEMresumen;
// opcioness
var opc_chat_disp = localStorage.getItem("opc_chat_disp") || false || "auto";
var opc_mano_disp = localStorage.getItem("opc_mano_disp") || false || "auto";
var EM_cat='all';
var EM_orden='set';
//var manoop=[0,0,0,0,0];
var denales=0;
var cartastotales=[];
var cartasenmazo=[];
var nombredelmazo="";
var organizador_de_familias=[];

function devolverNumeroFamilia(img) {
	var base, ret;
	if (arr_recards[img-1].tipo!='p') {
		return 0;
	} else if (arr_recards[img-1].subtipo=='3') {
		var buscar=arr_recards[img-1].evolvesfrom;
		base=arr_recards.find(v=>{return (v.nombre==buscar)}).img;
		return devolverNumeroFamilia(base);
	} else if (arr_recards[img-1].subtipo=='2') {
		var buscar=arr_recards[img-1].evolvesfrom;
		base=arr_recards.find(v=>{return (v.nombre==buscar)}).img;
		return devolverNumeroFamilia(base);
	} else if (arr_recards[img-1].subtipo=='0') {
		var buscar=arr_recards[img-1].evolvesfrom;
		if (buscar.indexOf(';')!=-1)
			return -1;
		base=arr_recards.find(v=>{return (v.nombre==buscar)}).img;
		return devolverNumeroFamilia(base);
	} else {
		base=arr_recards[img-1].nombre;
		ret=organizador_de_familias.indexOf(base);
		if (ret==-1) {
			ret=organizador_de_familias.length;
			organizador_de_familias.push(base);
		}
		return ret;
	}
}

function devolverCategoria(precio) {
	var ret='s';
	for (v in nivel_categorias) {
		if (precio<nivel_categorias[v]&&nivel_categorias[ret]>nivel_categorias[v])
			ret=v;
	}
	return ret;
}

function estadosToStr(src,est,val) {
	var enc=0;
	var letra=est;
	src=src.split('').filter(v=>v!=letra);
	src=src.concat(Array(val).fill(letra));
	return src.join('');
}

function echoCarta(img, siz, lado, opc) {
    if (img == -1) return "./resources/images/slotVacio.png";
    return rutaimg + (siz ? "../cards/" : "") + img + ".jpg";
}

function newConn(h, p) {
    if (ws_status != 0) return;
    ws_status = 1;
    if (!h) h = location.href.substr(0, 4) == 'file' ? defaultHost : location.hostname;
    if (!p) p = 8001;

    connection = new WebSocket("wss://" + h + ":" + p);
    connection.onopen = function() {
		console.log("+++ Connection opened");
        document.body.bgColor = '#8a8';
        ws_status = 2;
    }
    connection.onclose = function() {
		console.log("--- Connection closed");
        document.body.bgColor = '#888';
        ws_status = 0;
    }
    connection.onerror = function(e) {
        console.error("--- Connection error", e);
        document.body.bgColor = '#a88';
        ws_status = 0;
    }
    connection.onmessage = interpretarDatos;
}

function enviarDatos(str) {
	lastenvio=Date.now();
	console.log("OUT: " + str);
    if (ws_status == 2)
        connection.send(str);
}

function interpretarDatos(e) {
	lastrecep=Date.now();
    if (Date.now() < nextAnim) {
        setTimeout(function() { volverAInterpretar(e); }, nextAnim - Date.now() + 10);
        return;
    }
	console.log("IN: " + e.data);
    var inn = JSON.parse(e.data);
    if (inn.show == 'login')
        enviarDatos(str_login);
    else if (inn.show == 'juego')
        enviarDatos(str_join);
	else if (inn.show == 'mazos') {
		verDiv("mazos");
		var cum="";
		cum+="<div class='centerMazo'>";
		for (var m=0;m<inn.mazos.length;m++) {
			cum+="<div class='itemMazo' id='mazo_"+inn.mazos[m].mid+"' onClick=\"goEditarMazo("+inn.mazos[m].mid+",'"+inn.mazos[m].nombre+"','"+inn.mazos[m].cartas+"','all')\";>";
				cum+="<div class='nombre'>"+inn.mazos[m].nombre+"</div>";
				cum+="<div class='cartas'>("+inn.mazos[m].cartas.split(";").length+")</div>";
			cum+="</div>";
		}
		cum+="</div>";
		$("#divlistamazos").html(cum);
	}
	else if (inn.error) {
        alert(inn.error.mensaje);
    }
}

sit = setInterval(function() {
    
}, 1000);

function goMenu(elem,desc){
	$('#menusuperior li.menu').removeClass('activa');$(elem).addClass('activa');
}

function verDiv(ver,force) {
	if (force||ver!=viendo) {
		if (ver=='mazos') {
			var cum="";
			cum+="<div class='headerMazos'>Administrar mazos</div>";
			cum+="<div id='divlistamazos'><div class='cargando'>Cargando...</div></div>";
			divtodo.html(cum);
		} else if (ver=='editaMazo') {
			var cum="";
			cum+="<div id='diveditamazo'>";
				cum+="<div id='diveditamazo_cartas'>";
					cum+="<div id='diveditamazo_categs'>";
						cum+="<img src='resources/images/botonesMazo/all.png' class='editamazo_categ all' onclick=\"doEMchangeCat(this,'all');\">";
						cum+="<img src='resources/images/botonesMazo/energys.png' class='editamazo_categ n' onclick=\"doEMchangeCat(this,'n');\">";
						cum+="<img src='resources/images/botonesMazo/agua.png' class='editamazo_categ a' onclick=\"doEMchangeCat(this,'a');\">";
						cum+="<img src='resources/images/botonesMazo/psiquico.png' class='editamazo_categ p' onclick=\"doEMchangeCat(this,'p');\">";
						cum+="<img src='resources/images/botonesMazo/electrico.png' class='editamazo_categ e' onclick=\"doEMchangeCat(this,'e');\">";
						cum+="<img src='resources/images/botonesMazo/hoja.png' class='editamazo_categ h' onclick=\"doEMchangeCat(this,'h');\">";
						cum+="<img src='resources/images/botonesMazo/fuego.png' class='editamazo_categ f' onclick=\"doEMchangeCat(this,'f');\">";
						cum+="<img src='resources/images/botonesMazo/lucha.png' class='editamazo_categ l' onclick=\"doEMchangeCat(this,'l');\">";
						cum+="<img src='resources/images/botonesMazo/darkness.png' class='editamazo_categ o' onclick=\"doEMchangeCat(this,'o');\">";
						cum+="<img src='resources/images/botonesMazo/metal.png' class='editamazo_categ m' onclick=\"doEMchangeCat(this,'m');\">";
						cum+="<img src='resources/images/botonesMazo/colorless.png' class='editamazo_categ d' onclick=\"doEMchangeCat(this,'d');\">";
						cum+="<img src='resources/images/botonesMazo/trainers.png' class='editamazo_categ t' onclick=\"doEMchangeCat(this,'t');\">";
					cum+="</div>";
					cum+="<div id='diveditamazo_filtros'>";
						cum+="<input id='editamazo_chgorden' type='button' onclick='doEMcambiarOrden(this);' value='Por Expansión'>";
						cum+="<input id='editamazo_txtfiltro' type='text' value='' onkeyup='doEMfiltrarNombre(this.value);' placeholder='Filtrar...'>";
					cum+="</div>";
					cum+="<div id='diveditamazo_listacartas'>";
						cum+="<div class='cargando'>Cargando...</div>";
					cum+="</div>";
				cum+="</div>";
				cum+="<div id='diveditamazo_stat'>";
					cum+="c<br>a<br>r<br>g<br>g<br>d<br>n<br>d<br>o<br>.<br>.<br>.";
				cum+="</div>";
				cum+="<div id='diveditamazo_resumen'>";
					cum+="<div class='nombre_mazo'>"+nombredelmazo+"</div>";
					cum+="<div id='diveditamazo_resumencartas'>";
						cum+="<div class='cargando'>Cargando...</div>";
					cum+="</div>"
				cum+="</div>";
			cum+="</div>";
			divtodo.html(cum);
			divEMcartas=$("#diveditamazo_listacartas");
			divEMstat=$("#diveditamazo_stat");
			divEMresumen=$("#diveditamazo_resumencartas");
		}
	}
	viendo=ver;
}

function goEditarMazo(mid,nombre,cartas,tocat) {
	//cartastotales="";
	nombredelmazo=nombre;
	cartasenmazo=compilarCartas(cartas);
	verDiv("editaMazo",true);
	if (tocat)
		doEMchangeCat(null,tocat,true);
	dibujarEMcartas();
	dibujarEMresumen();
}

function doEMchangeCat(elem,to,skipdraw) {
	if (!elem)
		elem=document.querySelector('#diveditamazo_categs .editamazo_categ.'+to);
	$('#diveditamazo_categs .editamazo_categ').removeClass('activa');
	if (elem) {
		$(elem).addClass('activa');
	}
	
	if (EM_cat&&EM_cat!=to) {
		EM_cat=to;
		if (!skipdraw)
			dibujarEMcartas();
	}
}

function doEMcambiarOrden(elem,skipdraw) {
	var mostrar,neval;
	if (EM_orden=='set') {
		neval='nombre';
		mostrar="Por nombre";
	} else if (EM_orden=='nombre') {
		neval='precio';
		mostrar="Mayor precio";
	} else if (EM_orden=='precio') {
		neval='preciob';
		mostrar="Menor precio";
	} else {
		neval='set';
		mostrar="Por expansión";
	}
	
	if (!elem)
		elem=document.getElementById('editamazo_txtfiltro');
	elem.value=mostrar;
	EM_orden=neval;
	if (!skipdraw) {
		dibujarEMcartas();
	}
}

function doEMfiltrarNombre(txt) {
	txt=txt||document.getElementById('editamazo_txtfiltro').value||'';
	txt=txt.toLowerCase();
	var doms=divEMcartas[0].querySelectorAll(".cartaenlistacartas");
	for (var d=0;d<doms.length;d++) {
		if (txt.length==0||doms[d].getAttribute('buscar').indexOf(txt)>=0)
			doms[d].style.display='inline-block';
		else
			doms[d].style.display='none';
	}
}

function dibujarEMcartas() {
	var todibujar=(cartastotales.length<cartasenmazo.length?cartasenmazo:cartastotales).map((v,i)=>{return {img:i,cant:v}}).filter(v=>(v.cant>=1));
	//ordenar
	if (EM_orden=='set') {
		todibujar.forEach((v,i)=>{v.orden=arr_recards[v.img-1].sets;});
		todibujar.sort((a,b)=>{return b.orden>a.orden});
	} else if (EM_orden=='nombre') {
		todibujar.forEach((v,i)=>{v.orden=arr_recards[v.img-1].nombre;});
		todibujar.sort((a,b)=>{return b.orden<a.orden});
	} else if (EM_orden=='precio') {
		todibujar.forEach((v,i)=>{v.orden=arr_recards[v.img-1].precio;});
		todibujar.sort((a,b)=>{return b.orden-a.orden});
	} else if (EM_orden=='preciob') {
		todibujar.forEach((v,i)=>{v.orden=arr_recards[v.img-1].precio;});
		todibujar.sort((a,b)=>{return a.orden-b.orden});
	}
	//por categoria
	if (EM_cat=='n')
		todibujar=todibujar.filter(v=>{return (arr_recards[v.img-1].tipo=='e');});
	else if (EM_cat=='t')
		todibujar=todibujar.filter(v=>{return (arr_recards[v.img-1].tipo=='t');});
	else if (EM_cat!='all')
		todibujar=todibujar.filter(v=>{return (arr_recards[v.img-1].tipo=='p'&&arr_recards[v.img-1].color==EM_cat);});
	//fin categoria
	var cum="",img=0,tiene=0,maxim=0,total;
	todibujar.forEach((v,i)=>{
		img=v.img;
		tiene=entero(cartasenmazo[v.img]);
		maxim=arr_recards[img-1].maxcopias;
		total=entero(cartastotales[img]);
		cum+="<div id='celc_"+img+"' class='unsel cartaenlistacartas' onclick='EM_agregarcarta("+img+",this);event.stopPropagation();event.preventDefault();' img='"+img+"' tiene='"+tiene+"' maxim='"+maxim+"' total='"+total+"' buscar='"+arr_recards[img-1].nombre.toLowerCase()+"'>";
		cum+="<img src='"+echoCarta(img)+"' class='celc_carta' alt='#"+img+"#' title='"+arr_recards[img-1].nombre+"'>";
		cum+="<div class='celc_nombre'>"+arr_recards[img-1].nombre+"</div>";
		cum+="<div class='celc_cant' onclick='EM_quitarcarta("+img+",this);event.stopPropagation();event.preventDefault();'>...</div>";
		cum+="<div class='celc_precio'>"+enDenales(arr_recards[img-1].precio)+"</div>";
		cum+="</div>";
	});
	divEMcartas.html(cum);
	divEMcartas.find(".cartaenlistacartas").each((i,v)=>{
		EM_recant(v,'celc');
	});
	doEMfiltrarNombre();
}

function EM_recant(elem,clase) {
	if (clase=='celc') {
		var img=entero(elem.getAttributeNode('img').value);
		var tiene=entero(cartasenmazo[img]);
		var total=entero(cartastotales[img]);
		var maxim=arr_recards[img-1].maxcopias;
		var toedit=elem.querySelector("."+clase+"_cant");
		$(toedit).html("<font class='tiene'>"+tiene+"</font><font class='maxim'>/"+maxim+"</font> <font class='total'>("+total+")</font>");
	} else if (clase=='cerc') {
		var img=entero(elem.getAttributeNode('img').value);
		var tiene=entero(cartasenmazo[img]);
		var total=entero(cartastotales[img]);
		var maxim=maximo(total,arr_recards[img-1].maxcopias);
		var toedit=elem.querySelector("."+clase+"_cant");
		$(toedit).html("<font class='tiene'>"+tiene+"</font><font class='maxim'>/"+maxim+"</font>");
	}
}

function dibujarEMresumen() {
	var todibujar=cartasenmazo.map((v,i)=>{return {img:i,cant:v,orden:0}}).filter(v=>(v.cant>=1));
	//ordenar
	todibujar.forEach((v,i)=>{if (arr_recards[v.img-1].tipo=='e') v.orden=100-(v.img/100); else if (arr_recards[v.img-1].tipo=='t') {v.orden=1000-(v.img/100)-(arr_recards[v.img-1]=='t'?200:(arr_recards[v.img-1]=='o'?400:600));} else {v.orden=100000-devolverNumeroFamilia(v.img)*20-entero();} });
	todibujar.sort((a,b)=>{return b.orden-a.orden});
	//filtrar
	var cum="",sum="",img=0,tiene=0,maxim=0;
	todibujar.forEach((v,i)=>{
		img=v.img;
		tiene=v.cant;
		maxim=arr_recards[img-1].maxcopias;
		if (tiene) {
			cum+="<div id='cerc_"+img+"' class='unsel cartaenresumencartas' onclick='EM_quitarcarta("+img+",this);event.stopPropagation();event.preventDefault();' img='"+img+"' tiene='"+tiene+"' maxim='"+maxim+"'>";
				cum+="<div class='cerc_containerimg'>";
					cum+="<img src='"+echoCarta(img)+"' class='cerc_carta' alt='#"+img+"#' title='"+arr_recards[img-1].nombre+"'>";
				cum+="</div>";
				cum+="<div class='cerc_container'>";
					cum+="<div class='cerc_nombre'>"+arr_recards[img-1].nombre+"</div>";
					cum+="<div class='cerc_cant'>"+tiene+"/"+maxim+"</div>";
				cum+="</div>";
				cum+="<div class='cerc_containeradd' onclick='EM_agregarcarta("+img+",null);event.stopPropagation();event.preventDefault();'>";
					cum+="<img src='resources/images/editaMazo/add_plus.png' class='add_carta' alt='#"+img+"#' title='Añadir "+arr_recards[img-1].nombre+"'>";
				cum+="</div>";
			cum+="</div>";
		}
	});
	divEMresumen.html(cum);
	divEMresumen.find(".cartaenresumencartas").each((i,v)=>{
		EM_recant(v,'cerc');
	});
	dibujarEMstat();
}

function dibujarEMstat() {
	var s_tipos={todas:0,e:{b:0,s:0},et:0,p:{"0":0, "1":0, "2": 0, "3": 0},pt:0,t:{c:0, t:0, s:0, o: 0},tt:0};
	var s_precio=0;
	var todibujar=cartasenmazo;
	//ordenar
	//filtrar
	var cum="",img=0,tiene=0,maxim=0;
	todibujar.forEach((v,i)=>{
		img=i;
		tiene=v;
		//stats
		if (s_tipos[arr_recards[img-1].tipo]&&typeof s_tipos[arr_recards[img-1].tipo][arr_recards[img-1].subtipo]!='undefined') {
			s_tipos[arr_recards[img-1].tipo][arr_recards[img-1].subtipo]+=tiene;
			s_tipos[arr_recards[img-1].tipo+'t']+=tiene;
		}
		s_tipos.todas+=tiene;
		s_precio+=tiene*arr_recards[img-1].precio;
	});
	//dibujar stats
	cum+="<div class='icono deck'><img src='resources/images/editaMazo/deck.png' title='Total de cartas en Mazo'><br>"+s_tipos.todas+"</div>";
	cum+="<div class='icono energys'><img src='resources/images/editaMazo/energys.png' title='Total de energías Bas/Sp'><font class='alladoicono'>x"+s_tipos['et']+"</font><br>"+s_tipos['e']['b']+"/"+s_tipos['e']['s']+"</div>";
	cum+="<div class='icono pokes'><img src='resources/images/editaMazo/pokemon.png' title='Total de pokémon Baby/Bas/St1/st2'><font class='alladoicono'>x"+s_tipos['pt']+"</font><br>"+s_tipos['p']['0']+"/"+s_tipos['p']['1']+"/"+s_tipos['p']['2']+"/"+s_tipos['p']['3']+"</div>";
	cum+="<div class='icono trainers'><img src='resources/images/editaMazo/trainers.png' title='Total de trainers Com/Stad/Tool/Sup'><font class='alladoicono'>x"+s_tipos['tt']+"</font><br>"+s_tipos['t']['c']+"/"+s_tipos['t']['t']+"/"+s_tipos['t']['o']+"/"+s_tipos['t']['s']+"</div>";
	cum+="<div class='icono categ'><font class='tit_categ' title='Categoria y Precio Total'>Cat."+devolverCategoria(s_precio).toUpperCase()+"</font><br>"+enDenales(s_precio)+"</div>";
	divEMstat.html(cum);
}

function EM_agregarcarta(img,elem) {
	var tiene=entero(cartasenmazo[img]);
	var maxim=arr_recards[img-1].maxcopias;
	var total=entero(cartastotales[img]);
	
	if (tiene<maxim&&tiene<total) {
		cartasenmazo[img]=tiene+1;
		//cartaenlistacartas
		if (!elem||elem.className.indexOf("cartaenlistacartas")==-1) {
			elem=document.getElementById("celc_"+img);
		}
		if (elem)
			EM_recant(elem,'celc');
		//cartaenresumencartas
		if (tiene) {
			var nelem=document.getElementById("cerc_"+img);
			if (nelem)
				EM_recant(nelem,'cerc');
			dibujarEMstat();
		} else {
			dibujarEMresumen();
		}
	}
}

function EM_quitarcarta(img,elem) {
	var tiene=entero(cartasenmazo[img]);
	var maxim=arr_recards[img-1].maxcopias;
	var total=entero(cartastotales[img]);
	
	if (tiene) {
		cartasenmazo[img]=tiene-1;
		//cartaenlistacartas
		if (!elem||elem.className.indexOf("cartaenlistacartas")==-1) {
			elem=document.getElementById("celc_"+img);
		}
		if (elem)
			EM_recant(elem,'celc');
		//cartaenresumencartas
		if (tiene-1) {
			var nelem=document.getElementById("cerc_"+img);
			if (nelem)
				EM_recant(nelem,'cerc');
			dibujarEMstat();
		} else {
			dibujarEMresumen();
		}
	}
}

function compilarCartas(cartas,opc) {
	var ret=[];
	if (cartas!='') {		
		var li=cartas.split(";");
		var c=1,d;
		li.forEach((v,i)=>{
			d=v.split('x');
			v=d[0];
			c=d.length>1?parseInt(d[1]):1;
			ret[v]=ret[v]?ret[v]+=c:c;
		});
	}
	return ret;
}

function doResize() {
    screen_w = divtodo[0].offsetWidth;
    screen_h = divtodo[0].offsetHeight;
    screen_ver = screen_h > screen_w;
    //cambiarTodo(opc_chat_disp, opc_mano_disp, screen_ver);
}
$(window).resize(function() {
    doResize();
});
$(document).ready(function() {
	document.body.onmousedown = function() { clickdown=1; lastclick=Date.now(); }
	document.body.onmouseup = function() { clickdown=0; lastclick=Date.now(); }
	document.body.ontouchstart = function(e) { if (e.touches.length==1) clickdown=1; lastclick=Date.now(); }
	document.body.ontouchend = function(e) { if (e.touches.length==0) clickdown=0; lastclick=Date.now(); }
	//divchatlogs=$("#divchatlogs");
	divtodo=$("#todocontainer");
    $(window).resize();
    // cambiarTodo(opc_chat_disp,opc_mano_disp,null);
    $(document).keypress(keypress);
    //newConn();
	//interpretarDatos({data:'{"show":"mazos","mazos":[{"mid":"1","img":"1","nombre":"fulano","cartas":"9;9;8;7"},{"mid":"2","img":"1","nombre":"mengano","cartas":"9;9;8;7;6"}]}'});
	cartastotales=compilarCartas("9;9;9;8;5;4;3;55;54;1;2;57;59;60;61;62;63;64;65;65;7;9;9;9;8;5;4;3;55;54;1;2;57;59;60;61;62;63;64;65;65;7;41;42;44;99;100;102");
	goEditarMazo(1,'Nombre del Mazo','9;9;9;8;5;4;3;55;54;1;2;57;59;60;61;62;63;64;65;65;7','all');
});


//evs
function keypress(e) {
    var preventEscape = false;
    if (!kbfocus) {
        if (e.key == ' ') {
            $("#divmanoductl").trigger("mouseenter");
            e.preventDefault();
        }
    } else if (kbfocus == 'mano') {
        if (e.key == 'Escape') {
            $("#divmanoductl").trigger("mouseleave");
            e.preventDefault();
            preventEscape = true;
        } else if (e.key == ' ') {
            $("#divmanoductl").trigger("mouseleave");
            e.preventDefault();
        }
        /* else if (e.key=='ArrowRight') {
        			var seles=$("#divmanoductl .carta_mano");
        			var sl=seles.find(".selected");
        			if (sl.length==0) {
        				seles.first().addClass("selected");
        			} else if (sl.length>=2) {
        				sl.removeClass("selected");
        				seles.first().addClass("selected");
        			}
        		}*/
    } else if (kbfocus == 'elecciones') {
        if (e.key == 'Escape') {
            $("#divelecciones")[0].onperform("_cancel");
            e.preventDefault();
            preventEscape = true;
        }
    }
    //en cualquier parte
    if (e.key == 'Escape') {
        if ($(".focused,.selected").length > 0) {
            select();
            $(".focused").removeClass("focused");
            preventEscape = true;
        }
        /*if (!preventEscape) {
            if (!$("#divtodobatalla").hasClass("conchat"))
                divchatlogs.fadeToggle(animDelay * 2);
        }*/
        e.preventDefault();
    } else if (e.key == 'F2') {
        cambiarTodo(!$("#divtodobatalla").hasClass("conchat"), null);
        e.preventDefault();
    } else if (e.key == 'F3') {
        cambiarTodo(null, !$("#divtodobatalla").hasClass("conmano"));
        e.preventDefault();
    } else {
        //console.log(e);
    }
}

function select(e) {
    $(".dragover").removeClass("dragover");
    if (!e) {
        $(".selected").removeClass("selected");
        return;
    }
    if ($(e.target).hasClass("selected")) {
        $(e.target).removeClass("selected");
    } else {
        //var seles = $("#divbatalla .selected");
        var seles = $(".selected");
        if (nidselect == 0 && seles.length >= 1) {
            var dg = seles[0].getAttribute("draginfo");
            if (!dg)
                dg = $(seles[0]).find(".dragCarta")[0].getAttribute("draginfo");
            var dp = e.target.getAttribute("draginfo");
            if (!dp)
                dp = $(e.target).find(".dragCarta")[0].getAttribute("draginfo");
            lastCartaUsada = seles.first().parents(".root_carta").first();
            seles.removeClass("selected");
            interpretarAccion("click", dg, dp);
        } else {
            $(e.target).addClass("selected");
        }
    }
    e.stopPropagation();
}

function ctxMenu(e) {
    var arr=[];
    var tosend=[];
    var sele=$(e.target);

	arr.push("Ver cartas");
    tosend.push("--view-pos");
    if (sele.attr('draginfo').split(';')[0]=='b0du') {
        arr.push("Atacar");
        tosend.push("getAtaques");
    }
    if (sele.attr('draginfo').split(';')[0].substr(2,2)=='du') {
		var carta=parseInt(sele.attr('draginfo').split(';')[1].split(':')[0]);
		var pkpw=arr_pkpw.find(function(v){return v.carta==carta});
		if (pkpw) {
			arr.push(pkpw.nombre);
			tosend.push("pokepower "+sele.attr('draginfo').split(';')[0].substr(0,2)+" "+pkpw.pkpw);
		}
    }
    if (arr.length>0) {
        showElecciones("botones", arr, tosend, {needToPerform:1,dontFadeOut:true}, (ee,v)=>{if (v=='--view-pos') {ee.preventDefault();showCartasDePos(sele.parents(".root_carta")[0].parentNode,{texto:"Cartas:"});}else{showElecciones();}});
    }
    e.stopPropagation();
    e.preventDefault();
}

function allowDrop(e) {
    e.preventDefault();
}

function drag(e) {
    $(".dragover").removeClass("dragover");
    var dg = e.target.getAttribute("draginfo");
    e.dataTransfer.setData("text", dg);
	var dimg=$(e.target).attr("dragimg");
    if (dimg) {
        var img = document.createElement("img");
        img.src = dimg;
        e.dataTransfer.setDragImage(img, img.width / 2, img.height / 2);
    }
    lastCartaUsada = $(e.target).parents(".root_carta").first();
    if (dg.split(";")[0] == "mano")
        $("#divmanoductl").trigger("mouseleave");
}

function drop(e) {
    e.preventDefault();
    var dg = e.dataTransfer.getData("text");
    var dp = e.target.getAttribute("draginfo");
    if (!dp)
        dp = $(e.target).find(".dragCarta")[0].getAttribute("draginfo");
    e.stopPropagation();
    $(".dragover").removeClass("dragover");
    interpretarAccion("drop", dg, dp);
}

function interpretarAccion(tipo, fr, to, opc) {
    showElecciones();
    var frs = fr.split(";");
    var tos = to.split(";");
    if (frs[0] == "mano") {
        if (["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(tos[0].substr(0, 2)) != -1) {
            enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
        }
    } else if (frs[0] == "responde") {
        enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
    } else if (frs[0] == "b0du" && tos[0] == "b0op") { //proximo a eliminarse
        enviarDatos("accmesa getAtaques");
    } else if (frs[0] != tos[0] && ["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(frs[0].substr(0, 2)) != -1 && ["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(tos[0].substr(0, 2)) != -1) {
        enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
    }
	clickdown=0;
}

function maxWindow() {
    var tofu = document.getElementById('divtodobatalla');
    if (tofu.webkitRequestFullScreen) tofu.webkitRequestFullScreen();
    else tofu.mozRequestFullScreen();

    isFullScreen = true;
    //document.getElementById('audio_m').play();
}
/*helpers*/
function maximo(n, m) {
    if (n > m) return m;
    else return n;
}

function minimo(n, m) {
    if (n < m) return m;
    else return n;
}

function rand(n, m) {
    return (n + Math.round((m - n) * Math.random()));
}

function enReloj(num) {
    num = minimo(Math.floor(num), 0);
    var mi = Math.floor(num / 60);
    var se = num % 60;
    if (mi < 10) mi = '0' + mi;
    if (se < 10) se = '0' + se;
    return (mi + ':' + se);
}
