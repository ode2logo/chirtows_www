
function maximo(n, m) {
    if (n > m) return m;
    else return n;
}

function minimo(n, m) {
    if (n < m) return m;
    else return n;
}

function rand(n, m) {
    return (n + Math.round((m - n) * Math.random()));
}

function enReloj(num) {
    num = minimo(Math.floor(num), 0);
    var mi = Math.floor(num / 60);
    var se = num % 60;
    if (mi < 10) mi = '0' + mi;
    if (se < 10) se = '0' + se;
    return (mi + ':' + se);
}

function entero(num) {
	num=parseInt(num);
	if (isNaN(num))
		num=0;
	return num;
}

function enDecimal(num,pos,ennumero) {
	var mult=1,ret;
	for (var p=0;p<pos;p++) {
		mult*=10;
	}
	num=entero(num*mult)/mult;
		
	if (ennumero)
		return num;
	
	num=num+'';
	ret=num.split('.');
	ret[1]=ret[1]+'000000';
	ret[1]=ret[1].substr(0,pos);
	
	return ret[0]+'.'+ret[1];
}

function enDenales(num,puretxt) {
	ret=enDecimal(num,2);
	
	if (puretxt)
		return ret;
	
	ret=ret.split('.');
	
	return "<font class='endenales'>"
			+"<font class='den_sim'>Ð$</font>"
			+"<font class='den_val'>"
				+"<font class='den_int'>"+ret[0]+"</font>"
				+"<font class='den_dec'>."+ret[1]+"</font>"
			+"</font>"
		+"</font>";
}