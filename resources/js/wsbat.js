var defaultHost = location.hostname || "ws.chirto.com.ar";
var screen_w = 0;
var screen_h = 0;
var screen_ver = false;
var ws_status = 0;
var ldu = 0;
var lop = 1;
var team = 0;
var hs=location.hash.substr(1).split('|');
var mesa = hs[0];
var str_login = "login "+hs[1]+" "+hs[2];
var str_join = 'join mesa ' + mesa;
var str_refresh = 'accmesa refreshTabla';
var animDelay = 100;
var animSmLado = false;
var animSmPos = false;
var animCtl, touchCtl;
var nextAnim = Date.now();
var tabla = { "id": -1, "modo": "", "desde": 0, "now": 0, "hasta": 0, "etapa": 0, "turno": 0, "turnode": -1, "tiempode": -1, "tiempo": [0, 0], "players": [{ "dueno": 0, "cuenta": "---", "imagen": "esperando_jugador.png", "clan": 0, "color": "bebebe" }, { "dueno": 0, "cuenta": "---", "imagen": "esperando_jugador.png", "clan": 0, "color": "bebebe" }], "cartas": [{ m: 0, h: [], d: [], p: [null,null,null,null,null,null], t: [], s: [], w:[], b0: [], b1: [], b2: [], b3: [], b4: [], b5: [] }, { m: 0, h: [], d: [], p: [null,null,null,null,null,null], t: [], s: [], w: [], b0: [], b1: [], b2: [], b3: [], b4: [], b5: [] }] };
var lastCartaUsada = null;
var lastclick=Date.now();
var clickdown = 0;
var lastenvio=Date.now();
var lastrecep=Date.now();
var lasttapartodo="";
var kbfocus = null;
var rutaimg = "http://img.chirto.com.ar/img/cards2/";
// var rutaimg="";
var vidablanca = "<img src=resources/images/vidablanca.gif width=15%/>";
var vidanegra = "<img src=resources/images/vidanegra.gif width=15%/>";
var img_estados = {};
	img_estados['c']="<img src=resources/images/confundido.gif width=15%/>";
	img_estados['d']="<img src=resources/images/dormido.gif width=15%/>";
	img_estados['p']="<img src=resources/images/paralizado.gif width=15%/>";
	img_estados['v']="<img src=resources/images/envenenado.gif width=15%/>";
	img_estados['q']="<img src=resources/images/quemado.gif width=15%/>";
	img_estados['i']="<img src=resources/images/congelado.gif width=15%/>";
	img_estados['u']="<img src=resources/images/puno.gif width=15%/>";
	img_estados['f']="<img src=resources/images/escudo.gif width=15%/>";
var let_estados = {'confundido':'c','dormido':'d','paralizado':'p','envenenado':'v','quemado':'q','congelado':'i'};

var defProv=[null,'a','p','e','h','f','l','dd'];
var nidselect = 0;
var sis = [];
var sit;
var posPlayerOp = { width: "10%", height: "10%", top: "-30%", left: "30%" };
var posPlayerDu = { width: "10%", height: "10%", top: "100%", left: "60%" };
//doms
var divtodobatalla;
var divchatlogs;
var divtapatodo;
// opcioness
var opc_chat_disp = localStorage.getItem("opc_chat_disp") || false || "auto";
var opc_mano_disp = localStorage.getItem("opc_mano_disp") || false || "auto";
//tests
var var_rdy_pokes = false;
var autocoin = false;
var skipcoin = false;
//var manoop=[0,0,0,0,0];


function AnimController() {
    this.animando = { j: 0 };
    this.colaAnimar = { j: [] };
    this.evaluate = function(inn) {
		var inn=inn;
        var opc;
        var holdpos = null;
        var anim;
        if (inn.key == 'tabla') {
            holdpos = ["j"];
            anim = function() { makeTabla(function() { animCtl.callbackFromAnim(holdpos) }) };
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirAtaque') {
            holdpos = ["ee"];
            var atasNombres = inn.ataques.map(v => "<div class='ataReqen'>" + v.reqen.split("").map(v=>"<img src='resources/images/"+v+".png'>").join("") + "</div><div class='ataNombre'><div class='centrado'>" + v.nombre + "</div></div><div class='ataAtaque'>" + v.ataque + '</div>');
            var atasAccions = inn.ataques.map(v => "atacar " + v.id);
			if (inn.pendiente) {
                anim = function() {
                    showElecciones("ataques", atasNombres, atasAccions, { prePerform: ['responde'], notIncomplete: true, texto: inn.texto || '' });
                    animCtl.callbackFromAnim(holdpos)
                };
			} else {
				//atasNombres.push("Pasar turno");
				//atasAccions.push("pasarTurno");
				anim = function() {
					showElecciones("ataques", atasNombres, atasAccions, { notIncomplete: true });
					animCtl.callbackFromAnim(holdpos)
				};
			}
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirColor') {
            holdpos = ["ee"];
            var colsNombres = inn.colores.map(v => "<div class='ataReqen'><img src='resources/images/"+v+".png'></div><div class='ataNombre'>" + toLang.trans('color_'+v) + '</div>');
            var colsAccions = inn.colores;
			if (inn.pendiente) {
                anim = function() {
                    showElecciones("ataques", colsNombres, colsAccions, { prePerform: ['responde'], notIncomplete: true, texto: inn.texto || '' });
                    animCtl.callbackFromAnim(holdpos)
                };
			}
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirOpciones') {
            holdpos = ["ee"];
            if (inn.pendiente)
                anim = function() {
                    showElecciones("botones", inn.opcionesTexto, inn.opcionesCmd, { prePerform: ['responde'], notIncomplete: true, texto: inn.texto || '' });
                    animCtl.callbackFromAnim(holdpos)
                };
            else
                anim = function() {
                    showElecciones("botones", inn.opcionesTexto, inn.opcionesCmd, { notIncomplete: true });
                    animCtl.callbackFromAnim(holdpos)
                };
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirCartas') {
            holdpos = ["ee"];
            if (inn.pendiente)
                anim = function() {
                    showElecciones("cartas", inn.opciones, null, { prePerform: ['responde'], modoPerform: inn.modo, needToPerform: inn.cantidad, mustBeConfirmed: true, notIncomplete: (!!inn.cantidad), texto: inn.texto || '', otrasCartas: inn.otrasCartas });
                    animCtl.callbackFromAnim(holdpos)
                };
            else
                anim = function() {
                    showElecciones("botones", inn.opcionesTexto, inn.opcionesCmd, { notIncomplete: true });
                    animCtl.callbackFromAnim(holdpos);
                };
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirPrices') {
            holdpos = ["ee"];
            anim = function() {
                showElecciones("prices", null, null, { lado:inn.tolado, lugares:inn.lugares, notIncomplete:true, texto: inn.texto || '' });
                animCtl.callbackFromAnim(holdpos);
            };
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'elegirPokemon') {
            holdpos = ["ee"];
            anim = function() {
                showElecciones("pokemon", null, null, { lado:inn.tolado, lugares:inn.lugares, notIncomplete:true, texto: inn.texto || '' });
                animCtl.callbackFromAnim(holdpos);
            };
            this.add(holdpos, anim, opc);
		} else if (inn.key == 'elegirMovimiento') {
            holdpos = ["ee"];
            anim = function() {
                showElecciones("pokemon", null, null, { lado:inn.tolado, lugares:inn.lugares, movimiento:true, notIncomplete:true, texto: inn.texto || '' });
                animCtl.callbackFromAnim(holdpos);
            };
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'player_fuera') {
			holdpos = ["ee"];
            anim  = function() {
				inn.desc.forEach(v=>plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,toLang.trans(v)])));
				animCtl.callbackFromAnim(holdpos);
			}
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'rdy_player') {
			holdpos = ["ee"];
            anim  = function() {
				if (!tabla.cartas)
					tabla.cartas = [];
				if (!tabla.cartas[inn.lado])
					tabla.cartas[inn.lado] = { m: 0, h: [], d: [], p: [], b0: [], b1: [], b2: [], b3: [], b4: [], b5: [] };
				plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
				animCtl.callbackFromAnim(holdpos);
			}
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'rdy_pokes') {
			holdpos = ["ee"];
            anim = function() {
                plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
                animCtl.callbackFromAnim(holdpos);
            };
            this.add(holdpos, anim, opc);
            if (inn.lado == ldu)
                var_rdy_pokes = true;
        } else if (inn.key == 'mesa_etapa') {
            holdpos = ["ee"];
            anim  = function() {
				tabla.etapa = parseInt(inn.desc.split('->')[1]);
				if (tabla.etapa==3) {
					showElecciones();
				}
				animCtl.callbackFromAnim(holdpos);
			};
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'set_reloj') {
			holdpos = ["ee"];
            anim  = function() {
				tabla.desde = inn.desc.desde;
				tabla.now = inn.desc.now;
				tabla.tiempode = inn.desc.tiempode;
				tabla.hasta = inn.desc.hasta;
				if (tabla.tiempode!=-1) {
					tabla.tiempo[tabla.tiempode]=inn.desc.ntiempo;
				}
				animCtl.callbackFromAnim(holdpos);
			};
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'set_turno') {
            holdpos = ["ee"];
            anim  = function() {
				plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc]));
				animCtl.callbackFromAnim(holdpos);
			};
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'team_ganador') {
            holdpos = ["ee"];
			anim  = function() {
				plog(toLang.trans(inn.key));
				showGanador({ganador:inn.desc},{},function() { animCtl.callbackFromAnim(holdpos) }); 
			};
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'error') {
            holdpos = ["ee"];
            anim  = function() { showError(inn.errorTxt,{},function() { animCtl.callbackFromAnim(holdpos) }); };
            this.add(holdpos, anim, opc);
		} else if (inn.key == 'moneda') {
			holdpos = ["ee"];
			var titi={log:null,res:inn.desc.res.split("")};
			for (var m=0;m<inn.desc.res.length;m++) {
				anim  = function() {
					if (!titi.log)
						titi.log=plog((inn.desc.by.acc?"Por "+inn.desc.by.acc+". ":"")+"Moneda: <b></b>.");
					showElecciones("moneda", [], null, {cara:(titi.res.shift()=='O'),lado:inn.lado,texto:"Tirando moneda",appendToLog:titi.log.find('b')}, function() { animCtl.callbackFromAnim(holdpos); });
				};
				this.add(holdpos, anim, opc);
			}
        } else if (inn.key == 'ini_mazo') {
            if (inn.lado == lop) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
					iniMazo(inn.desc, true, function() { animCtl.callbackFromAnim(holdpos) })
				};
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
					iniMazo(inn.desc, false, function() { animCtl.callbackFromAnim(holdpos) })
				};
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].m = inn.desc;
        } else if (inn.key == 'manoAlDeck') {
            var nm=parseInt(tabla.cartas[inn.lado].m)+parseInt(tabla.cartas[inn.lado].h.length);
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos = [inn.lado + "h"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
					manoAlDeck(true, function() { redrawMazo(nm,inn.lado==lop); makeMano([],false,inn.lado==lop); animCtl.callbackFromAnim(holdpos); });
				};
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].m = nm;
            tabla.cartas[inn.lado].h = [];
        } else if (inn.key == 'baraja') {
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
					redrawMazo(inn.desc,inn.lado==lop); barajarMazo(inn.lado==lop, function() { animCtl.callbackFromAnim(holdpos) })
				};
            }
            this.add(holdpos, anim, opc);
        } else if (inn.key == 'robaCartas' && inn.desc) {
            inn.priv=inn.priv||Array(inn.desc).fill(0);
            if (inn.lado == lop) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc]));
					robarCartas(inn.desc, true, function() { makeMano(tabla.cartas[inn.lado].h, false, true); animCtl.callbackFromAnim(holdpos); });
				};
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc]));
					robarCartas(inn.desc, false, function() { animCtl.callbackFromAnim(holdpos) }, function(c) { makeMano([inn.priv[c - 1]], true, false); })
				};
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].m -= inn.desc;
            tabla.cartas[inn.lado].h = tabla.cartas[inn.lado].h.concat(inn.priv);
		} else if (inn.key == 'mueve:m:h' && inndesc.cartas.length) {
            inn.priv=inn.priv||Array(inn.desc.cartas.length).fill(0);
            if (inn.lado == lop) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,'#'+inn.desc.cartas.join('# #')+'#']));
					robarCartas(inn.desc.cartas.length, true, function() { makeMano(tabla.cartas[inn.lado].h, false, true); animCtl.callbackFromAnim(holdpos); });
				};
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + "m"];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
					robarCartas(inn.desc.cartas.length, false, function() { animCtl.callbackFromAnim(holdpos) }, function(c) { makeMano([inn.priv[c - 1]], true, false); })
				};
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].m -= inn.desc.cartas.length;
            tabla.cartas[inn.lado].h = tabla.cartas[inn.lado].h.concat(inn.priv.cartas||inn.desc.cartas);
		} else if (inn.key == 'mueve:d:h') {
			if (inn.lado == lop||inn.lado == ldu) {
				holdpos = [inn.lado + "d"];
				anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
					for (var i=0;i<inn.desc.cartas.length;i++) {
						var dino=$("#divdesc"+(inn.lado==lop?'op':'du')+" .cartaImgD[img="+inn.desc.cartas[i]+"]");
						var dyn={img:inn.desc.cartas[i]};
						if (inn.lado==lop)
							dyn.fu=(i==inn.desc.cartas.length-1?function(){makeMano([0],true,(inn.lado==lop));redrawDiscard('label', inn.lado == lop);animCtl.callbackFromAnim(holdpos);}:function(){makeMano([0],true,(inn.lado==lop));});
						else
							dyn.fu=(i==inn.desc.cartas.length-1?function(){makeMano([this.img],true,(inn.lado==lop));redrawDiscard('label', inn.lado == lop);animCtl.callbackFromAnim(holdpos);}:function(){makeMano([this.img],true,(inn.lado==lop));});
						if (dino.length) {
							turnIntoAnimaCarta(dino.first(), (inn.lado==lop?posPlayerOp:posPlayerDu), {preAnimWait:i}, dyn.fu.bind(dyn));
						} else {
							animCtl.callbackFromAnim(holdpos);
							callRefreshTabla();
							break;
						}
					}
				}
			}
			this.add(holdpos, anim, opc);
			for (var i=0;i<inn.desc.cartas.length;i++) {
				var tst=inn.desc.cartas.indexOf(inn.desc.cartas[i]);
				tabla.cartas[inn.lado].d.splice(minimo(tst,0),1);
			}
            tabla.cartas[inn.lado].h = tabla.cartas[inn.lado].h.concat(inn.priv.cartas||inn.desc.cartas);
		} else if (inn.key=='mueve:h:m' && inn.desc.cartas.length) {
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + 'm'];
				if (inn.lado==lop) {
					anim = function () {
						plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
						var last=maximo($("#divmanoop .cartaImgH").length,inn.desc.cartas.length);
						var c=0;
						$("#divmanoop .cartaImgH").each(function(i,v){
							if (i<last) {
								makeAnimaCarta(echoCarta(0,false,lop),posPlayerOp, $("#divmazoop"), {efectoResize:0.4,preAnimWait:i*0.5}, function(){
									//mazo se arregla con la siguiente animacion
									c++;
									if(c==last) {
										animCtl.callbackFromAnim(holdpos);
									}
								});
								$("#divmanoop .carta_man0 .cartaImgH").first().remove();
							}
							redrawMani(null,true);
						});
					}
				} else {
					inn.desc.cartas=inn.priv.cartas||inn.desc.cartas;
					anim = function () {
						plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
						var last=maximo($("#divmanoduhid .carta_mano").length,inn.desc.cartas.length);
						var c=0;
						var ndesc=inn.desc.cartas.filter(v=>v);
						$("#divmanoduhid .carta_mano").each(function(i,v){
							if (ndesc.indexOf(parseInt($(this).attr('img')))!=-1) {
								var d=ndesc.splice(ndesc.indexOf(parseInt($(this).attr('img'))),1);
								turnIntoAnimaCarta($(this), $("#divmazodu"), {efectoResize:0.4,preAnimWait:i*0.5}, function(){
									//mazo
									c++;
									if(c==last) {
										animCtl.callbackFromAnim(holdpos);
									}
								});
							}
						});
						redrawMano();
					}
				}
                this.add(holdpos, anim, opc);
            }
            for (var i=0;i<inn.desc.cartas.length;i++) {
                var tst=tabla.cartas[inn.lado]['h'].indexOf(inn.desc.cartas[i]);
                if (tst==-1)
                    tst=0;
                tabla.cartas[inn.lado]['h'].splice(tst,1);
            }
            tabla.cartas[inn.lado].m+=inn.desc.cartas.length;
		} else if (inn.key.substr(0,7)=='mueve:b' && inn.key.substr(8,2)==':b' && inn.desc.cartas.length) {
			var frpos=inn.key.substr(6,2);
			var topos=inn.key.substr(9,2);
			if (inn.lado == lop || inn.lado == ldu) {
				holdpos=[inn.lado + frpos];
				inn.desc.cartas=inn.priv.cartas||inn.desc.cartas;
				var frposla=frpos+(inn.lado==lop?'op':'du');
                var toposla=topos+(inn.lado==lop?'op':'du');
                anim = function () {
					for (var i=0;i<inn.desc.cartas.length;i++) {
						var dyn={img:inn.desc.cartas[i],fu:function(){}}
						if (i==inn.desc.cartas.length-1) {
							dyn.fu=function(){
								makePoke([{img:this.img}],toposla,true);
								redrawPoke([],frposla);
								redrawEnergias(inn.desc.fpok,frposla);
								redrawEnergias(inn.desc.tpok,toposla);
								animCtl.callbackFromAnim(holdpos);
							}
						} else {
							dyn.fu=function(){makePoke([{img:this.img}],toposla,true);}
						}
						turnIntoAnimaCarta($("#div"+frposla+" .carta_poke.root_carta[img="+inn.desc.cartas[i]+"]").first(), $("#div"+toposla), {preAnimWait:i*1}, dyn.fu.bind(dyn) );
					}
                }
                this.add(holdpos, anim, opc);
			}
			for (var i=0;i<inn.desc.cartas.length;i++) {
                var tst=tabla.cartas[inn.lado][frpos].indexOf(inn.desc.cartas[i]);
                if (tst==-1)
                    tst=0;
				tabla.cartas[inn.lado][frpos].splice(tst,1);
            }
			tabla.cartas[inn.lado][topos]=tabla.cartas[inn.lado][topos].concat(inn.priv.cartas||inn.desc.cartas);
        } else if (inn.key == 'ponePrices') {
            holdpos = [inn.lado + "m"];
            anim = function() { ponerPrices(inn.desc, (inn.lado == lop), function() { animCtl.callbackFromAnim(holdpos) }); };
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].m -= inn.desc.length;
            tabla.cartas[inn.lado].p = inn.desc;
        }  else if (inn.key == 'robaPrice') {
            holdpos = [inn.lado + "m"];
            anim = function() {
				plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta]));
				robarPrice(inn.desc.ind, (inn.lado == lop), function() { makeMano([(inn.priv||inn.desc.img)], true, (inn.lado == lop)); animCtl.callbackFromAnim(holdpos) });
			};
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].p[inn.desc.ind]=-1;
            tabla.cartas[inn.lado].h.push(inn.priv||inn.desc.img);
        } else if (inn.key == 'revelaPokes') {
            for (var i in inn.desc) {
                var cas = inn.desc[i];
                var holdpos;
                for (var bx in cas) {
                    var toimg = cas[bx][0].img || null;
                    if (i == lop) {
                        if (toimg) {
                            holdpos = [inn.lado + bx];
                            var c = { holdpos: holdpos, pobj: cas[bx], bx: bx, toimg: toimg };
                            anim = function() {
                                turnIntoCartaFlip($("#div" + this.bx + "op .fixInner"), echoCarta(this.toimg, false, lop), null, function() {
                                    makePoke(this.pobj, this.bx + 'op');
                                    animCtl.callbackFromAnim(this.holdpos)
                                }.bind(this));
                            };
                            this.add(holdpos, anim.bind(c), opc);
                        } else {
                            holdpos = null;
                            makePoke(cas[bx], bx + 'op'); /*?*/
                        }
                    } else if (i == ldu) {
                        if (toimg) {
                            holdpos = [inn.lado + bx];
                            var c = { holdpos: holdpos, pobj: cas[bx], bx: bx, toimg: toimg };
                            anim = function() {
                                turnIntoCartaFlip($("#div" + this.bx + "du .fixInner"), echoCarta(this.toimg, false, ldu), null, function() {
                                    makePoke(this.pobj, this.bx + 'du');
                                    animCtl.callbackFromAnim(this.holdpos);
                                }.bind(this));
                            };
                            this.add(holdpos, anim.bind(c), opc);
                        } else {
                            holdpos = null;
                            makePoke(cas[bx], bx + 'du'); /*?*/
                        }
                    } else {
                        holdpos = null;
                    }
                    tabla.cartas[i][bx] = cas[bx];
                }
            }
        } else if (inn.key == 'cambiaPoke') {
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + inn.desc.fpos];
                var frposla=inn.desc.fpos+(inn.lado==lop?'op':'du');
                var toposla=inn.desc.tpos+(inn.lado==lop?'op':'du');
                var pokf=tabla.cartas[inn.lado][inn.desc.fpos].map(v=>v);
                var pokt=tabla.cartas[inn.lado][inn.desc.tpos].map(v=>v);
                anim = function () {
					if (!tabla.cartas[inn.lado][inn.desc.tpos][0].nombre)
						plog(toLang.trans('cambiaPoke-1',[tabla.cartas[inn.lado][inn.desc.fpos][0].nombre]));
					else
						plog(toLang.trans('cambiaPoke',[tabla.cartas[inn.lado][inn.desc.tpos][0].nombre]));
                    swapPoke(frposla,toposla,{},function () {makePoke(pokf,toposla); makePoke(pokt,frposla); animCtl.callbackFromAnim(holdpos)});
                }
                this.add(holdpos, anim, opc);
            }
            var tmp=tabla.cartas[inn.lado][inn.desc.fpos].splice(0);
            tabla.cartas[inn.lado][inn.desc.fpos]=tabla.cartas[inn.lado][inn.desc.tpos].splice(0);
            tabla.cartas[inn.lado][inn.desc.tpos]=tmp.splice(0);
		} else if (inn.key == 'ataca') {
			if (inn.lado == lop || inn.lado == ldu) {
				holdpos=[inn.lado + 'b0'];
				var frposla='b0'+(inn.lado==lop?'op':'du');
                var toposla='b0'+(inn.desc.tolado==lop?'op':'du');
				anim = function () {
					plog(toLang.trans(inn.key,[tabla.cartas[inn.lado].b0[0].nombre,inn.desc.ataNombre]));
                    shakePos(frposla, 'wipe', {}, function(){animCtl.callbackFromAnim(holdpos);});
                }
                this.add(holdpos, anim, opc);
			}
		} else if (inn.key == 'pokepower') {
			if (inn.lado == lop || inn.lado == ldu) {
				holdpos=[inn.lado + 'b0'];
				anim = function () {
					plog(toLang.trans(inn.key,[tabla.cartas[inn.lado][inn.desc.from][0].nombre,inn.desc.nombre]));
					animCtl.callbackFromAnim(holdpos);
                }
                this.add(holdpos, anim, opc);
			}
		} else if (inn.key == 'trainer'&&inn.desc.from=='h') {
			if (inn.lado == lop) {
                holdpos = [inn.lado + 'w'];
                anim = function() {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.nombre]));
                    makeAnimaCarta(echoCarta(inn.desc.img, false, lop), posPlayerOp, $("#divprizop").first(), {dontRemove:true,reClass:'carta_w op',animDelay:6,animWait:5}, function() {
                        //makePoke(inn.desc, topos + 'op');
                        animCtl.callbackFromAnim(holdpos);
                    });
                    $("#divmanoop .carta_man0 .cartaImgH").first().remove();
                    redrawMani(null,true);
                }
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + 'w'];
				if (!lastCartaUsada||lastCartaUsada.attr('img')!=inn.desc.img) {
					lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img="+inn.desc.img+"]").first();
					if (!lastCartaUsada.length) {
						lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img=0]").first();
					}
				}
                if (lastCartaUsada) {
                    var lcu = lastCartaUsada;
                    anim = function() {
						plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.nombre]));
                        turnIntoAnimaCarta(lcu, $("#divprizdu").first(), {dontRemove:true,reClass:'carta_w du',animWait:5}, function() {
                            //makePoke(inn.desc, topos + 'du');
                            animCtl.callbackFromAnim(holdpos);
                        });
                        redrawMano();
                    }
                    lastCartaUsada = null;
                }
            }
			this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].h.splice(minimo(tabla.cartas[inn.lado].h.indexOf(inn.desc.img),0), 1);
            tabla.cartas[inn.lado].w.push(inn.desc.img);
		} else if (inn.key == 'cancelTrainer'&&inn.desc.from=='h') {
			if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + 'w'];
                anim = function () {
					plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.nombre]));
					turnIntoAnimaCarta($(".carta_w."+(inn.lado==lop?'op':'du')), (inn.lado==lop?posPlayerOp:posPlayerDu), {}, function() {
						makeMano([inn.lado==lop?0:inn.desc.img],true,inn.lado==lop)
						animCtl.callbackFromAnim(holdpos);
					});
                }
                this.add(holdpos, anim, opc);
            }
            for (var i=0;i<inn.desc.length;i++) {
                var tst=tabla.cartas[inn.lado]['w'].indexOf(inn.desc.img);
                if (tst==-1)
                    tst=0;
                tabla.cartas[inn.lado]['w'].splice(tst,1);
            }
            tabla.cartas[inn.lado].h=tabla.cartas[inn.lado].h.concat([inn.desc.img]);
        } else if (inn.key.substr(0, 10) == 'descarta:w') {
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + 'w'];
                anim = function () {
					turnIntoAnimaCarta($(".carta_w."+(inn.lado==lop?'op':'du')), $("#divdesc"+(inn.lado==lop?'op':'du')).first(), {efectoResize:0.4}, function() {
						//makePoke(inn.desc, topos + 'du');
						redrawDiscard(inn.desc.cartas, (inn.lado==lop), true);
						animCtl.callbackFromAnim(holdpos);
					});
                }
                this.add(holdpos, anim, opc);
            }
            for (var i=0;i<inn.desc.length;i++) {
                var tst=tabla.cartas[inn.lado]['w'].indexOf(inn.desc[i]);
                if (tst==-1)
                    tst=0;
                tabla.cartas[inn.lado]['w'].splice(tst,1);
            }
            tabla.cartas[inn.lado].d=tabla.cartas[inn.lado].d.concat(inn.desc);
        } else if (inn.key == 'descarta:h' && inn.desc.cartas.length) {
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + 'h'];
				if (inn.lado==lop) {
					anim = function () {
						plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
						var last=maximo($("#divmanoop .cartaImgH").length,inn.desc.cartas.length);
						var c=0;
						$("#divmanoop .cartaImgH").each(function(i,v){
							if (i<last) {
								makeAnimaCarta(echoCarta(0,false,lop),posPlayerOp, $("#divdescop"), {efectoResize:0.4,preAnimWait:i*0.5}, function(){
									redrawDiscard([inn.desc.cartas[i]], (inn.lado==lop), true);
									c++;
									if(c==last) {
										animCtl.callbackFromAnim(holdpos);
									}
								});
								$("#divmanoop .carta_man0 .cartaImgH").first().remove();
							}
							redrawMani(null,true);
						});
					}
				} else {
					anim = function () {
						plog(toLang.trans(inn.key,[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', ')]));
						var last=maximo($("#divmanoduhid .carta_mano").length,inn.desc.cartas.length);
						var c=0;
						var ndesc=inn.desc.cartas.filter(v=>v);
						$("#divmanoduhid .carta_mano").each(function(i,v){
							if (ndesc.indexOf(parseInt($(this).attr('img')))!=-1) {
								var d=ndesc.splice(ndesc.indexOf(parseInt($(this).attr('img'))),1);
								turnIntoAnimaCarta($(this), $("#divdescdu"), {efectoResize:0.4,preAnimWait:i*0.5}, function(){
									redrawDiscard(d, (inn.lado==lop), true);
									c++;
									if(c==last) {
										animCtl.callbackFromAnim(holdpos);
									}
								});
							}
						});
						redrawMano();
					}
				}
                this.add(holdpos, anim, opc);
            }
            for (var i=0;i<inn.desc.cartas.length;i++) {
                var tst=tabla.cartas[inn.lado]['h'].indexOf(inn.desc.cartas[i]);
                if (tst==-1)
                    tst=0;
                tabla.cartas[inn.lado]['h'].splice(tst,1);
            }
            tabla.cartas[inn.lado].d=tabla.cartas[inn.lado].d.concat(inn.desc.cartas);
        } else if (inn.key == 'pokeKO') {
			plog(toLang.trans(inn.key,[toLang.trans('pos_'+inn.desc.pos),inn.desc.nombre]));
        } else if (inn.key.substr(0, 10) == 'descarta:b' && inn.desc.cartas.length) {
            var topos = inn.key.substr(9);
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + topos];
                var toposla=topos+(inn.lado==lop?'op':'du');
                anim = function () {
					if (tabla.cartas[inn.lado][topos][0].nombre)
						plog(toLang.trans(inn.key.substr(0, 10),[tabla.players[inn.lado].cuenta,'#'+inn.desc.cartas.join('# #')+'#',tabla.cartas[inn.lado][topos][0].nombre]));
                    makeDescartar(inn.desc.cartas,toposla,{},function(){
						redrawPoke([],toposla);
						redrawEnergias(inn.desc.pok,toposla);
						animCtl.callbackFromAnim(holdpos);
					});
                }
                this.add(holdpos, anim, opc);
            }
            tabla.cartas[inn.lado][topos]=inn.desc.pok;
            tabla.cartas[inn.lado].d=tabla.cartas[inn.lado].d.concat(inn.desc.cartas);
		} else if (inn.key.substr(0, 7) == 'mueve:b' && inn.key.substr(9) == 'h' && inn.desc.cartas.length) {
            var frpos = inn.key.substr(6,2);
            if (inn.lado == lop || inn.lado == ldu) {
                holdpos=[inn.lado + frpos];
				inn.desc.cartas=inn.priv.cartas||inn.desc.cartas;
                var frposla=frpos+(inn.lado==lop?'op':'du');
                anim = function () {
					plog(toLang.trans("mueve:b:h",[tabla.players[inn.lado].cuenta,inn.desc.cartas.join(', '),tabla.cartas[inn.lado][frpos][0].nombre]));
					for (var i=0;i<inn.desc.cartas.length;i++) {
						var dyn={img:inn.desc.cartas[i],fu:function(){makeMano([inn.lado==lop?0:this.img],true,inn.lado==lop);}}
						if (i==inn.desc.cartas.length-1) {
							dyn.fu=function(){
								makeMano([inn.lado==lop?0:this.img],true,inn.lado==lop);
								redrawPoke([],frposla);
								redrawEnergias(inn.desc.fpok,frposla);
								animCtl.callbackFromAnim(holdpos);
							}
						}
						turnIntoAnimaCarta($("#div"+frposla+" .carta_poke.root_carta[img="+inn.desc.cartas[i]+"]").first(), (inn.lado==lop?posPlayerOp:posPlayerDu), {preAnimWait:i*0.5}, dyn.fu.bind(dyn) );
					}
                }
                this.add(holdpos, anim, opc);
            }
            for (var i=0;i<inn.desc.cartas.length;i++) {
                var tst=tabla.cartas[inn.lado][frpos].indexOf(inn.desc.cartas[i]);
                if (tst==-1)
                    tst=1;
                tabla.cartas[inn.lado][frpos].splice(tst,1);
            }
            if (tabla.cartas[inn.lado][frpos].length==1) //fix cuando se queda sin nada
                tabla.cartas[inn.lado][frpos]=[];
            tabla.cartas[inn.lado].h=tabla.cartas[inn.lado].h.concat(inn.desc.cartas);
        } else if (inn.key.substr(0, 11) == 'bajaPoke:h:') {
            var topos = inn.key.substr(11);
            if (inn.lado == lop) {
                holdpos = [inn.lado + topos];
                anim = function() {
					plog(toLang.trans("bajaPoke:h:b",[tabla.players[inn.lado].cuenta,inn.desc[0].nombre,toLang.trans('pos_'+topos)]));
                    makeAnimaCarta(echoCarta(0, false, lop), posPlayerOp, $("#div" + topos + "op").first(), {}, function() {
                        makePoke(inn.desc, topos + 'op');
                        animCtl.callbackFromAnim(holdpos);
                    });
                    $("#divmanoop .carta_man0 .cartaImgH").first().remove();
                    redrawMani(null,true);
                }
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				inn.priv=inn.priv||inn.desc;
				if (!lastCartaUsada||lastCartaUsada.attr('img')!=inn.priv[0].img) {
					lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img="+inn.priv[0].img+"]").first();
					if (!lastCartaUsada.length) {
						lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img=0]").first();
					}
				}
                if (lastCartaUsada) {
                    var lcu = lastCartaUsada;
                    anim = function() {
						plog(toLang.trans("bajaPoke:h:b",[tabla.players[inn.lado].cuenta,inn.desc[0].nombre,toLang.trans('pos_'+topos)]));
                        turnIntoAnimaCarta(lcu, $("#div" + topos + "du").first(), null, function() {
                            makePoke(inn.desc, topos + 'du');
                            animCtl.callbackFromAnim(holdpos);
                        });
                        redrawMano();
                    }
                    lastCartaUsada = null;
                }
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado].h.splice(tabla.cartas[inn.lado].h.indexOf((inn.priv ? inn.priv.img : 0) || inn.desc.img), 1);
            tabla.cartas[inn.lado][topos] = inn.priv || inn.desc;
		} else if (inn.key.substr(0, 11) == 'bajaPoke:w:') {
			var topos = inn.key.substr(11);
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				var toposla=topos+(inn.lado==lop?"op":"du");
                anim = function() {
					plog(toLang.trans("bajaPoke:h:b",[tabla.players[inn.lado].cuenta,inn.desc[0].nombre,topos]));
                    turnIntoAnimaCarta($(".carta_w."+(inn.lado==lop?"op":"du")).first(), $("#div"+toposla).first(), null, function() {
                        makePoke(inn.desc, toposla);
                        animCtl.callbackFromAnim(holdpos);
                    });
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].w.indexOf(inn.desc[0].img) != -1)
                tabla.cartas[inn.lado].w.splice(tabla.cartas[inn.lado].w.indexOf(inn.desc[0].img), 1);
            else
                tabla.cartas[inn.lado].w.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc;
		} else if (inn.key.substr(0, 11) == 'bajaPoke:d:') {
			var topos = inn.key.substr(11);
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				var toposla=topos+(inn.lado==lop?"op":"du");
                anim = function() {
					plog(toLang.trans("bajaPoke:d:b",[tabla.players[inn.lado].cuenta,inn.desc[0].nombre,topos]));
                    turnIntoAnimaCarta($("#divdesc"+(inn.lado==lop?"op":"du")+" .cartaImgD[img="+inn.desc[0].img+"]").first(), $("#div"+toposla).first(), null, function() {
						redrawDiscard('label', inn.lado == lop, false);
                        makePoke(inn.desc, toposla);
                        animCtl.callbackFromAnim(holdpos);
                    });
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].d.indexOf(inn.desc[0].img) != -1)
                tabla.cartas[inn.lado].d.splice(tabla.cartas[inn.lado].d.indexOf(inn.desc[0].img), 1);
            else
                tabla.cartas[inn.lado].d.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc;
		} else if (inn.key.substr(0, 12) == 'refreshPoke:') {
			var topos = inn.key.substr(12);
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				var toposla=topos+(inn.lado==lop?"op":"du");
                anim = function() {
					plog(toLang.trans("refreshPoke",[tabla.cartas[inn.lado][topos][0].nombre,topos]));
                    makePoke(inn.desc, toposla);
                    animCtl.callbackFromAnim(holdpos);
                }
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado][topos] = inn.desc;
        } else if (inn.key.substr(0, 14) == 'poneAdjunta:h:') {
            var topos = inn.key.substr(14);
            if (inn.lado == lop) {
                holdpos = [inn.lado + topos];
                anim = function() {
					plog(toLang.trans("poneAdjunta:h:b",[tabla.players[inn.lado].cuenta,'#'+inn.desc.adj+'#',tabla.cartas[inn.lado][topos][0].nombre]));
                    makeAnimaCarta(echoCarta(inn.desc.adj, false, lop), posPlayerOp, $("#div" + topos + "op").first(), {}, function() {
                        makePoke([{img:inn.desc.adj}], topos + 'op', true, {nEnerg:inn.desc.pok[0].energias});
                        animCtl.callbackFromAnim(holdpos);
                    });
                    $("#divmanoop .carta_man0 .cartaImgH").first().remove();
                    redrawMani(null,true);
                }
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				if (!lastCartaUsada||lastCartaUsada.attr('img')!=inn.desc.adj) {
					lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img="+inn.desc.adj+"]").first();
					if (!lastCartaUsada.length) {
						lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img=0]").first();
					}
				}
                if (lastCartaUsada) {
                    var lcu = lastCartaUsada;
                    anim = function() {
						plog(toLang.trans("poneAdjunta:h:b",[tabla.players[inn.lado].cuenta,'#'+inn.desc.adj+'#',tabla.cartas[inn.lado][topos][0].nombre]));
                        turnIntoAnimaCarta(lcu, $("#div" + topos + "du").first(), null, function() {
                            makePoke([{img:inn.desc.adj}], topos + 'du', true, {nEnerg:inn.desc.pok[0].energias});
                            animCtl.callbackFromAnim(holdpos);
                        });
                        redrawMano();
                    }
                    lastCartaUsada = null;
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].h.indexOf(inn.desc.adj) != -1)
                tabla.cartas[inn.lado].h.splice(tabla.cartas[inn.lado].h.indexOf(inn.desc.adj), 1);
            else
                tabla.cartas[inn.lado].h.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc.pok;
        } else if (inn.key.substr(0, 14) == 'poneAdjunta:w:') {
            var topos = inn.key.substr(14);
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				var toposla=topos+(inn.lado==lop?"op":"du");
                anim = function() {
					plog(toLang.trans("poneAdjunta:h:b",[tabla.players[inn.lado].cuenta,'#'+inn.desc.adj+'#',tabla.cartas[inn.lado][topos][0].nombre]));
                    turnIntoAnimaCarta($(".carta_w."+(inn.lado==lop?"op":"du")).first(), $("#div"+toposla).first(), null, function() {
                        makePoke([{img:inn.desc.adj}], toposla, true, {nEnerg:inn.desc.pok[0].energias});
                        animCtl.callbackFromAnim(holdpos);
                    });
                    //redrawMano();
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].w.indexOf(inn.desc.adj) != -1)
                tabla.cartas[inn.lado].w.splice(tabla.cartas[inn.lado].w.indexOf(inn.desc.adj), 1);
            else
                tabla.cartas[inn.lado].w.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc.pok;
		} else if (inn.key.substr(0, 14) == 'poneAdjunta:d:') {
            var topos = inn.key.substr(14);
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
				var toposla=topos+(inn.lado==lop?"op":"du");
                anim = function() {
					plog(toLang.trans("poneAdjunta:d:b",[tabla.players[inn.lado].cuenta,'#'+inn.desc.adj+'#',tabla.cartas[inn.lado][topos][0].nombre]));
                    turnIntoAnimaCarta($("#divdesc"+(inn.lado==lop?"op":"du")+" .cartaImgD[img="+inn.desc.adj+"]").first(), $("#div"+toposla).first(), null, function() {
                        makePoke([{img:inn.desc.adj}], toposla, true, {nEnerg:inn.desc.pok[0].energias});
                        animCtl.callbackFromAnim(holdpos);
                    });
                    //redrawMano();
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].w.indexOf(inn.desc.adj) != -1)
                tabla.cartas[inn.lado].w.splice(tabla.cartas[inn.lado].w.indexOf(inn.desc.adj), 1);
            else
                tabla.cartas[inn.lado].w.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc.pok;
        } else if (inn.key.substr(0, 17) == 'evolucionaPoke:h:') {
            var topos = inn.key.substr(17);
            if (inn.lado == lop) {
                holdpos = [inn.lado + topos];
                anim = function() {
					plog(toLang.trans("evolucionaPoke:h:b",[tabla.players[inn.lado].cuenta,tabla.cartas[inn.lado][topos][0].nombre,inn.desc.pok[0].nombre]));
                    makeAnimaCarta(echoCarta(inn.desc.evo, false, lop), posPlayerOp, $("#div" + topos + "op").first(), {}, function() {
                        makePoke([inn.desc.pok[0]], topos + 'op','bottom');
                        animCtl.callbackFromAnim(holdpos);
                    });
                    $("#divmanoop .carta_man0 .cartaImgH").first().remove();
                    redrawMani(null,true);
                }
            } else if (inn.lado == ldu) {
                holdpos = [inn.lado + topos];
                if (!lastCartaUsada||lastCartaUsada.attr('img')!=inn.desc.evo) {
					lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img="+inn.desc.evo+"]").first();
					if (!lastCartaUsada.length) {
						lastCartaUsada=$("#divmanoductl .root_carta.carta_mano[img=0]").first();
					}
				}
                if (lastCartaUsada) {
                    var lcu = lastCartaUsada;
                    //console.log(lcu);
                    anim = function() {
						plog(toLang.trans("evolucionaPoke:h:b",[tabla.players[inn.lado].cuenta,tabla.cartas[inn.lado][topos][0].nombre,inn.desc.pok[0].nombre]));
                        turnIntoAnimaCarta(lcu, $("#div" + topos + "du").first(), null, function() {
                            makePoke([inn.desc.pok[0]], topos + 'du','bottom');
                            animCtl.callbackFromAnim(holdpos);
                        });
                        redrawMano();
                    }
                    lastCartaUsada = null;
                }
            }
            this.add(holdpos, anim, opc);
            if (tabla.cartas[inn.lado].h.indexOf(inn.desc.evo) != -1)
                tabla.cartas[inn.lado].h.splice(tabla.cartas[inn.lado].h.indexOf(inn.desc.evo), 1);
            else
                tabla.cartas[inn.lado].h.splice(0, 1);
            tabla.cartas[inn.lado][topos] = inn.desc.pok;
		} else if (inn.key.substr(0, 16) == 'devolucionaPoke:') {
            var topos = inn.key.substr(16);
			var toposla = topos + (inn.lado == lop?'op':'du');
            if (inn.lado == lop||inn.lado == ldu) {
                holdpos = [inn.lado + topos];
                anim = function() {
					plog(toLang.trans("devolucionaPoke:b",[tabla.players[inn.lado].cuenta,tabla.cartas[inn.lado][topos][0].nombre,inn.desc.pok[0].nombre]));
                    makePoke(inn.desc.pok, toposla);
                    animCtl.callbackFromAnim(holdpos);
                }
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado][topos] = inn.desc.pok;
        } else if (inn.key.substr(0, 11) == 'contadores:') {
            var topos = inn.key.substr(11);
            holdpos = [inn.lado + topos];
            var dano=inn.desc.tot;
            var cura=inn.desc.ini<0;
            if (inn.lado==lop||inn.lado==ldu) {
                var toposla=topos+(inn.lado==lop?'op':'du');
                var nhp=tabla.cartas[inn.lado][topos][0].hp;
                var nda=tabla.cartas[inn.lado][topos][0].dano+dano;
				var nst=tabla.cartas[inn.lado][topos][0].estados;
                anim = function() {
					if (cura)
						plog(toLang.trans("contadores-1",[topos,tabla.cartas[inn.lado][topos][0].nombre,(-dano)+'0']));
					else
						plog(toLang.trans("contadores",[topos,tabla.cartas[inn.lado][topos][0].nombre,(dano)+'0']));
					makeDano(toposla,(cura?-dano:dano)+'0',cura,{delayCb:4}, function () {redrawHP([{hp:nhp,dano:nda,estados:nst}],toposla);animCtl.callbackFromAnim(holdpos);});
				};
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado][topos][0].dano+=dano;
		} else if (inn.key.substr(0, 7) == 'estado:') {
            var topos = inn.key.substr(7);
            holdpos = [inn.lado + topos];
            var nestados=estadosToStr(tabla.cartas[inn.lado][topos][0].estados,let_estados[inn.desc.est],inn.desc.val);
            if (inn.lado==lop||inn.lado==ldu) {
				var toposla=topos+(inn.lado==lop?'op':'du');
				var nhp=tabla.cartas[inn.lado][topos][0].hp;
                var nda=tabla.cartas[inn.lado][topos][0].dano;
				var nst=nestados;
                if (inn.desc.val!=0)
					anim = function () { shakePos(toposla, 'shake', {}, function(){redrawHP([{hp:nhp,dano:nda,estados:nst}],toposla); animCtl.callbackFromAnim(holdpos);}) }
				else
					anim = function () { redrawHP([{hp:nhp,dano:nda,estados:nst}],toposla); animCtl.callbackFromAnim(holdpos);}
            }
            this.add(holdpos, anim, opc);
            tabla.cartas[inn.lado][topos][0].estados=nestados;
        }
        //finite
    }

    this.add = function(holdpos, anim, opc) {
        if (holdpos && holdpos.length) {
            if (holdpos[0] != 'j') {
                if (!animSmLado) //animacion simultanea a ambos lados
                    holdpos[0] = "e" + holdpos[0].substr(1);
                if (!animSmPos) //animacion simultanea distintas pos
                    holdpos[0] = holdpos[0].substr(0, 1) + "e";
            }
            if (typeof this.animando[holdpos[0]] == 'undefined') {
                this.animando[holdpos[0]] = 0;
                this.colaAnimar[holdpos[0]] = [];
            }
            this.colaAnimar[holdpos[0]].push({ holdpos: holdpos, anim: anim, opc: opc });
            this.check(holdpos);
        }
    }
    this.callbackFromAnim = function(holdpos) {
        this.animando[holdpos[0]] = 0;
        this.check(holdpos);
    }
    this.check = function(holdpos) {
        if (this.colaAnimar['j'].length) {
            if (this.isAnimando()) {
                if (holdpos[0] === 'j')
                    this.clearColas();
                console.log("a la fila " + holdpos[0]);
            } else {
                this.animando['j'] = 1;
                var an = this.colaAnimar['j'].shift();
                an.anim();
                console.log("ejecutando general " + holdpos[0]);
            }
        } else {
            if (!this.colaAnimar[holdpos[0]].length) {
                console.log("todo done " + holdpos[0]);
                if (holdpos[0] === 'j') {
                    var tan = this.getFaltaAnimar();
                    for (var i in tan) {
                        console.log("falto " + tan[i]);
                        this.check([tan[i]]);
                    }
                }
            } else if (this.animando[holdpos[0]] != 0 || this.animando['j'] != 0) {
                console.log("a la fila " + holdpos[0]);
            } else {
                this.animando[holdpos[0]] = 1;
                var an = this.colaAnimar[holdpos[0]].shift();
                an.anim();
                console.log("ejecutando " + holdpos[0]);
            }
        }
    }
    this.isAnimando = function() {
        for (var v in this.animando) {
            if (this.animando[v] != 0)
                return true;
        }
        return false;
    }
    this.getAnimando = function() {
        var ret = [];
        for (var v in this.animando) {
            if (this.animando[v] != 0)
                ret.push(v);
        }
        return ret;
    }
    this.getFaltaAnimar = function() {
        var ret = [];
        for (var v in this.colaAnimar) {
            if (v !== 'j' && this.colaAnimar[v].length > 0)
                ret.push(v);
        }
        return ret;
    }
    this.clearColas = function() {
        for (var v in this.colaAnimar) {
            if (v !== 'j' && this.colaAnimar[v].length > 0)
                this.colaAnimar[v] = [];
            else if (v === 'j' && this.colaAnimar[v].length > 1) {
                this.colaAnimar[v] = [this.colaAnimar[v].pop()];
            }
        }
    }
}
animCtl = new AnimController();

function estadosToStr(src,est,val) {
	var enc=0;
	var letra=est;
	src=src.split('').filter(v=>v!=letra);
	src=src.concat(Array(val).fill(letra));
	return src.join('');
}

function echoCarta(img, siz, lado, opc) {
    if (img == -1) return "./resources/images/slotVacio.png";
    return rutaimg + (siz ? "../cards/" : "") + img + ".jpg";
}

function ppCode(txt)
{
	txt=" "+txt+" ";
	var contitle;
	/*for (a=0;a<arr_smileys.length;a++)
	{
		txt=txt.split(" "+arr_smileys[a][0]+" ").join(" "+arr_smileys[a][1]+" ");
	}*/
	txt=txt.replace(/#(.*?)#/g,"<img src='http://chirto.com.ar/img/cards2/$1.jpg' title='#$1#' alt='#$1#' style='cursor:pointer;' onClick='original($1);' width='24' height='32'>");
	txt=txt.replace(/\[b\](.*?)\[\/b\]/g,"<b title='[b][/b]'>$1</b>");
	txt=txt.replace(/\[u\](.*?)\[\/u\]/g,"<u title='[u][/u]'>$1</u>");
	txt=txt.replace(/\[i\](.*?)\[\/i\]/g,"<i title='[i][/i]'>$1</i>");
	txt=txt.replace(/\[s\](.*?)\[\/s\]/g,"<s title='[s][/s]'>$1</s>");
	txt=txt.replace(/\[k\](.*?)\[\/k\]/g,"<blink title='[k][/k]'>$1</blink>");
	txt=txt.replace(/\[g\](.*?)\[\/g\]/g,"<font style='font-size:32px;' title='[g][/g]'>$1</font>");
	txt=txt.replace(/\[p\](.*?)\[\/p\]/g,"<img src='$1' title='[p][/p]'>");
	txt=txt.replace(/\[c=(.*?)\](.*?)\[\/c\]/g,"<span style='color:$1' title='[c=$1][/c]'>$2</span>");
	txt=txt.replace(/\[w\](.*?)\[\/w\]/g,"<a href='$1' target=_blank title='[w][/w]'>$1</a>");
	txt=txt.replace(/\[w=(.*?)\](.*?)\[\/w\]/g,"<a href='$1' target=_blank title='[w=][/w]'>$2</a>");
	return (txt.substr(1,txt.length-2));
}

function plog(txt,opc) {
	opc=opc||{};
	txt=ppCode(txt);
    var titi=$("<div class='"+(opc.class||'log')+"'><li>" + txt + "</li></div>").appendTo(divchatlogs);
    divchatlogs[0].scrollTop = divchatlogs[0].scrollHeight;
    //console.log(obj);
	return titi;
}

function newConn(h, p) {
    if (ws_status != 0) return;
    ws_status = 1;
    if (!h) h = location.href.substr(0, 4) == 'file' ? defaultHost : location.hostname;
    if (!p) p = 8001;
	
	taparTodo("error_wsconnecting");
	
    connection = new WebSocket("wss://" + h + ":" + p);
    connection.onopen = function() {
		console.log("+++ Connection opened");
        document.body.bgColor = '#8a8';
        ws_status = 2;
		taparTodo(-1);
    }
    connection.onclose = function() {
		console.log("--- Connection closed");
        document.body.bgColor = '#888';
        ws_status = 0;
		taparTodo("error_wsconnclose");
    }
    connection.onerror = function(e) {
        console.error("--- Connection error", e);
        document.body.bgColor = '#a88';
        ws_status = 0;
		taparTodo("error_wsconndie");
    }
    connection.onmessage = interpretarDatos;
}

function enviarDatos(str) {
	lastenvio=Date.now();
	console.log("OUT: " + str);
    if (ws_status == 2)
        connection.send(str);
}

function interpretarDatos(e) {
	lastrecep=Date.now();
    if (Date.now() < nextAnim) {
        setTimeout(function() { volverAInterpretar(e); }, nextAnim - Date.now() + 10);
        return;
    }
	console.log("IN: " + e.data);
    var inn = JSON.parse(e.data);
	if (inn.show == 'InternalError') {
		console.error(inn.message);
		alert(inn.message);
    } else if (inn.show == 'login') {
		if (inn.message=='Error user. Try logout.') {
			taparTodo("error_login");
		} else
			enviarDatos(str_login);
	} else if (inn.show == 'juego') {
        enviarDatos(str_join);
	} else if (inn.show == '-') {
		if (inn.message=='Falta mesa.')
			taparTodo("error_joining");
	}
    //hacer la magia
    if (inn.infoLado) {
        if (inn.infoLado.lado == 1) {
            ldu = 1;
            lop = 0;
            team: 1;
        } else {
            ldu = 0;
            lop = 1;
            team: 0;
        }
    } else if (inn.tipo == 'evento') {
        animCtl.evaluate(inn);
    } else if (inn.tabla) {
        tabla = inn.tabla;
        animCtl.evaluate({ key: 'tabla' });
    } else if (inn.ataques) {
        inn.key = 'elegirAtaque';
        animCtl.evaluate(inn);
    } else if (inn.pendiente) {
        if (inn.pendiente.decidir == 'numero') {
            inn.key = 'elegirOpciones';
            inn.opcionesTexto = [];
            inn.opcionesCmd = [];
            for (var c = inn.pendiente.min; c <= inn.pendiente.max; c++) {
                inn.opcionesTexto.push(c);
                inn.opcionesCmd.push(c);
            }
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'botones') {
            inn.key = 'elegirOpciones';
            inn.opcionesTexto = [];
            inn.opcionesCmd = [];
            inn.pendiente.nom.forEach(function (v,i) {
                inn.opcionesTexto.push(v);
                inn.opcionesCmd.push(inn.pendiente.acc[i]||v);
            });
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'descartar'||inn.pendiente.decidir == 'seleccionar') {
            inn.key = 'elegirCartas';
			if (inn.pendiente.cartas.length&&inn.pendiente.cartas[0].img) {
				inn.opciones = inn.pendiente.cartas.map(v => v.img);
			} else {
				inn.opciones = inn.pendiente.cartas;
			}
			inn.otrasCartas=inn.pendiente.otrasCartas||[];
            inn.cantidad = inn.pendiente.cant || 0;
            inn.modo = inn.pendiente.modo || 'seleccion';
            inn.texto = inn.pendiente.nombrePos + ": " + inn.texto;
            for (var c = inn.pendiente.min; c <= inn.pendiente.max; c++) {
                inn.opcionesTexto.push(c);
                inn.opcionesCmd.push(c);
            }
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'prices') {
            inn.key = 'elegirPrices';
            inn.tolado=inn.pendiente.lado;
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'pokemon') {
            inn.key = 'elegirPokemon';
            inn.lugares=inn.pendiente.lugares;
            inn.tolado=inn.pendiente.lado;
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'ataque') {
            inn.key = 'elegirAtaque';
            inn.ataques=inn.pendiente.ataques;
            animCtl.evaluate(inn);
        } else if (inn.pendiente.decidir == 'color') {
            inn.key = 'elegirColor';
            inn.colores=inn.pendiente.colores;
            animCtl.evaluate(inn);
		} else if (inn.pendiente.decidir == 'movimiento') {
            inn.key = 'elegirMovimiento';
            inn.lugares=inn.pendiente.lugares;
            inn.tolado=inn.pendiente.lado;
            animCtl.evaluate(inn);
		}
    } else if (inn.errorAcc) {
        animCtl.evaluate({key:'error',errorTxt:inn.errorAcc});
    }
}

function volverAInterpretar(e) {
    if (typeof e == 'string') {
        e = { data: e };
    }
    interpretarDatos(e);
}

function callRefreshTabla() {
    enviarDatos(str_refresh);
}

function makeZoom(idc) {
    window.open(echoCarta(idc, true));
}

function cambiarTodo(chat, mano, scrn) {
    if (chat != null) {
        if (chat === "auto")
            chat = scrn ? 1 : 0;
        if (typeof chat == "string")
            chat = parseInt(chat);
        chat ? divtodobatalla.addClass("conchat") : divtodobatalla.removeClass("conchat");
        if (chat) divchatlogs.fadeIn(animDelay * 2);
        opc_chat_disp = chat;
        localStorage.setItem("opc_chat_disp", chat ? 1 : 0);
    }
    if (mano != null) {
        if (mano === "auto")
            mano = scrn ? 1 : 0;
        if (typeof mano == "string")
            mano = parseInt(mano);
        mano ? divtodobatalla.addClass("conmano") : divtodobatalla.removeClass("conmano");
        opc_mano_disp = mano;
        localStorage.setItem("opc_mano_disp", mano ? 1 : 0);
    }
    if (scrn != null) {
        scrn ? divtodobatalla.addClass("conver") : divtodobatalla.removeClass("conver");
    }
    redrawMano();
    redrawMani(null, true);
    redrawPoke();
    redrawMazo();
    redrawDiscard();
    redrawStadium();
    redrawSuporter();
    redrawPrices();
}

function makeTabla(cb) {
    tabla.players.forEach(function (pl,l) {
        if (l==lop)
            redrawPlayer(pl, true);
        else if (l==ldu)
            redrawPlayer(pl);
    });
    tabla.cartas.forEach(function(ca, ll) {
        if (!ca)
            return;
        for (var p = 0; p <= 5; p++) {
            if (ll == ldu && ca["b" + p])
                makePoke(ca["b" + p], "b" + p + "du");
            else if (ll == lop && ca["b" + p])
                makePoke(ca["b" + p], "b" + p + "op");
        }
        if (ca.m!==null) {
            if (ll == ldu)
                redrawMazo(ca.m, false);
            else if (ll == lop)
                redrawMazo(ca.m, true);
        }
        if (ca.h) {
            if (ll == ldu)
                makeMano(ca.h, false, false);
            else if (ll == lop)
                makeMano(ca.h, false, true);
        }
        if (ca.d) {
            if (ll == ldu)
                redrawDiscard(ca.d, false, false);
            else if (ll == lop)
                redrawDiscard(ca.d, true, false);
        }
        if (ca.p) {
            if (ll == ldu)
                redrawPrices(ca.p, false);
            else if (ll == lop)
                redrawPrices(ca.p, true);
        }
        if (ca.t) {
            if (ll == ldu)
                redrawStadium(ca.t[0], false);
            else if (ll == lop)
                redrawStadium(ca.t[0], true);
        }
        if (ca.s) {
            if (ll == ldu)
                redrawSuporter(ca.s[0], false);
            else if (ll == lop)
                redrawSuporter(ca.s[0], true);
        }
    });
    if (!tabla.tiempo||!tabla.hasta) {
        tabla.tiempo=[600000,600000];
        tabla.now=Date.now();
        tabla.desde=Date.now();
        tabla.hasta=Date.now()+15000;
	    tabla.tiempode=0;
	    redrawTiempo();
	    tabla.tiempode=1;
	    redrawTiempo();
	    tabla.tiempode=-1;
	}
    if (cb) cb();
}

function checkEtapa() {
    if (!tabla)
        return;
    if (tabla.etapa == 0) {
        if (!tabla.cartas[ldu] || !tabla.cartas[ldu]['m'])
            //showElecciones("botones", ["Repartir cartas", "Cambiar mazo"], ["mazoListo", "alertar: Proximamente..."]); //next_to_lang
            showElecciones("botones", [toLang.trans("start_battle")], ["mazoListo"]); //next_to_lang
        else {
            //nada
        }
    }
    else if (tabla.etapa == 1 && !var_rdy_pokes) {
        if (!tabla.cartas[ldu] || !tabla.cartas[ldu]['b0'] || !tabla.cartas[ldu]['b0'][0] || typeof tabla.cartas[ldu]['b0'][0].img == 'undefined' || tabla.cartas[ldu]['b0'][0].img == -1) {
            if (!tabla.cartas[ldu] || !tabla.cartas[ldu]['h'] || !tabla.cartas[ldu]['h'].some(v => arr_cards[v - 1][1] == 'p' && arr_cards[v - 1][2] == '1')) {
                showElecciones("botones", [toLang.trans("no_have_basic")], ["volverARepartir"]); //next_to_lang
            } else {
                //nada
            }
        } else {
			if (Date.now()-lastclick>5000&&!clickdown) 
				showElecciones("botones", [toLang.trans("pokes_ready"), toLang.trans("pokes_not_ready")], ["pokesListos", "nulo"]); //next_to_lang
        }
    }
}
/***ANIMACIONES**/
function subiBajaMano(e) {
    if (divtodobatalla.hasClass("conmano"))
        return;
    if (!$(this).is(':animated')) {
        if (e && e.type == 'mouseenter') {
            $(this).animate({ bottom: 0 }, animDelay * 2).delay(animDelay);
            if (!kbfocus) kbfocus = 'mano';
        } else {
            $(this).find(".carta_mano.focused").animate({ top: 0 }, animDelay).removeClass('focused');
            $(this).animate({ bottom: "-28%" }, animDelay).delay(animDelay);
            if (kbfocus == 'mano') kbfocus = null;
        }
    }
}

function subiBajaCarta(e) {
    if (divtodobatalla.hasClass("conmano"))
        return;
    if (!$(this).is(':animated')) {
        if (e && e.type == 'mouseenter') {
            $("#divmanoduhid .carta_mano").not(this).animate({ top: 0 }, animDelay).removeClass('focused');
            $(this).finish().animate({ top: -screen_h * 0.4 * 0.05 }, animDelay * 2).addClass('focused');
        } else {
            $(this).animate({ top: 0 }, animDelay).removeClass('focused');
        }
    }
}
//draws
function makeMano(arr, append, lo) {
    if (!lo) {
        var hid = $("#divmanoduhid");
        var basei = 0;
        if (!append)
            hid.html("");
        else
            basei = hid.find(".carta_mano.root_carta").length;
        arr.forEach(function(v, i) {
            i += basei;
            hid.append("<div id='carta_mano_" + i + "' class='carta_mano root_carta' img='" + v + "'>"
				+ "<div class='fixContain'>"
					+ "<div class='fixInner'>"
						+ "<img class='cartaImg' src='" + echoCarta(v, false, ldu) + "'>"
							+ "<div class='overCarta'></div>"
							+ "<div class='dragCarta' onclick='select(event)' draggable='true' draginfo='mano;" + v + ":" + i + "' dragimg='" + echoCarta(v, false, ldu) + "' " + "ondblclick='makeZoom(" + v + ")' ondragstart='drag(event)' ondrop='drop(event)' ondragover='allowDrop(event)' " + "ondragenter=\"$(this).addClass('dragover')\" ondragleave=\"$(this).removeClass('dragover')\" ontouchstart=\"touchManager(event,'start',this);\" ontouchend=\"touchManager(event,'end',this);\"></div>"
						+ "</div>"
					+ "</div>"
				+ "</div>");
        });
        var hida = hid.find(".carta_mano");
        hida.on("mouseenter", subiBajaCarta)
            .on("mouseleave", subiBajaCarta);
        redrawMano(hida);
    } else {
        if (append) {
            arr.forEach(v=>{
                $("#divmanoop .carta_man0 .fixInner").append("<img class='cartaImgH' img='" + v + "' src='" + echoCarta(v, false, lo ? lop : ldu) + "'>");
            });
            redrawMani(null, lo);
        } else {
            redrawMani(arr, lo);
        }
    }
}

function redrawMano(hida) {
    if (!hida) hida = $("#divmanoduhid .carta_mano");
    var narr = [];
    if (hida.length) {
        var tx = 0,
            ty = 0;
        var dw = parseInt($("#divmanoductl").css('width'));
        var dh = parseInt($("#divmanoductl").css('height'));
        var wi = maximo(dh * 0.70, dw * 0.85);
        var dx = dw / (hida.length + 3);
        hida.stop().each(function(i) {
            $(this).attr("id", "carta_mano_" + i)
                .css({ width: wi, zIndex: hida.length - i });
            if (divtodobatalla.hasClass("conmano")) {
                if (divtodobatalla.hasClass("conver")) {
                    // tx=dw/2+((i+0.5)-hida.length/2)*dx-parseInt($(this).css('width'))/2;
                    var ttx = parseInt($(this).css('width'));
                    tx = ttx * i + dw / 2 - ttx * hida.length / 2;
                    tx = minimo(tx, ttx * i);
                    // alert($(this).css('width'));
                    $(this).animate({ left: tx, top: dh * 0.05 });
                } else {
                    ty = parseInt($(this).css('height')) * i;
                    $(this).animate({ top: ty, left: dw * 0.05 });
                }
            } else {
                tx = dw / 2 + ((i + 0.5) - hida.length / 2) * dx - parseInt($(this).css('width')) / 2;
                $(this).animate({ left: tx, top: 0 });
            }
            var im = $(this).attr("img");
            $(this).find(".dragCarta").attr("draginfo", "mano;" + im + ":" + i);
            narr.push(im);
        });
    }
    redrawMani(narr, false);
}

function redrawMani(arr, lo) {
    if (arr == null && lo == null) {
        redrawMani(null, false);
        redrawMani(null, true);
        return;
    }
    var man = lo ? $("#divmanoop") : $("#divmanodu");
    if (arr == null) {
        var narr = [];
        if (!lo) {
            $("#divmanoduhid .carta_mano").each(function(i) {
                var cim = $(this).attr("img");
                if (cim >= 0) narr.push(cim);
            });
        } else {
            man.find(".fixInner img.cartaImgH").each(function(i) {
                var cim = $(this).attr("img");
                if (cim >= 0) narr.push(cim);
            });
        }
        redrawMani(narr, lo);
        return;
    }
    var dw = parseInt(man.css('width'));
    var dh = parseInt(man.css('height'));
    var wi = dh;
    man.html("<div class='carta_man0 root_carta' style='width:" + wi + "px;max-width:100%;'>" + "<div class='fixContain'>" + "<div class='fixInner'></div>" + "</div>" + "<div class='label' style='position:relative;top:-60%'>" + arr.length + "x</div>" + "</div>");
    var manfi = man.find(".fixInner");
    if (arr.length == 0) {
        // var dw=parseInt(manfi.css('width'));
        // var dh=parseInt(manfi.css('height'));
        // manfi.append("<img class='cartaImgH' img='-1' src='"+echoCarta(-1,false,lo?lop:ldu)+"' style='right:"+Math.round(dw/2-dw*0.2)+"px;bottom:"+Math.round(dh/2-dw*0.2)+"px'>");
    } else {
        var lef, top, rot;
        for (var l = 1; l <= arr.length; l += 1) {
            lef = ((l - 0.5) / arr.length) * dw / 2;
            top = (Math.abs((l - 0.5) - arr.length / 2) / arr.length) * dh / 2;
            rot = -45 + ((l - 0.5) / arr.length) * 90;
            manfi.append("<img class='cartaImgH' img='" + arr[l - 1] + "' src='" + echoCarta(arr[l - 1], false, lo ? lop : ldu) + "' style='left:" + lef + "px;top:" + top + "px;transform:rotate(" + rot + "deg);-webkit-transform:rotate(" + rot + "deg);'>");
        }
        // var cini=parseInt(man.find("div.label").text());
        // man.find("div.label").text((manfi.find(".cartaImgH").length)+"x");
    }
}

function makePoke(arr, pos, append, opc) {
    var bid = $("#div" + pos);
    var over = "";
	if (!append)
		bid.html("");
    if (!arr || !arr[0] || arr[0].img == undefined)
        arr[0] = { img: -1, hp: 0, dano: 0, nombre: "---" };
    else if (arr.length > 1)
        arr=arr.filter((v,i)=>i!=1);
    // if (!arr||arr.length==0)
    // 	arr.push({img:-1,hp:0,dano:0,nombre:"---"});
    arr.forEach(function(v, i) {
        if (i != 0)
            v = { img: v, nombre: '' }
		var toape="<div id='carta_" + pos + "_" + i + "' class='carta_poke root_carta' img='" + v.img + "' style='" + (pos.substr(2, 2) == 'op' ? "right:0px" : "left:0px") + "'>"
                    + "<div class='fixContain'>"
                        + "<div class='fixInner'>"
                            + "<img class='cartaImg csf' img='" + v.img + "' src='" + echoCarta(v.img, false, pos.substr(2, 2) == 'op' ? lop : ldu) + "'>"
                                + "<div class='overCarta'></div>"
                                + "<div class='dragCarta' onclick='select(event)' draggable='true' draginfo='" + pos + ";" + v.img + ":" + i + "' dragimg='" + echoCarta(v.img, false, pos.substr(2, 2) == 'op' ? lop : ldu) + "' title='" + (v.nombre || '---') + "' "
                                    + "ondblclick='makeZoom(" + v.img + ")' ondragstart='drag(event)' ondrop='drop(event)' ondragover='allowDrop(event)' "
                                    + "ondragenter=\"$(this).addClass('dragover')\" ondragleave=\"$(this).removeClass('dragover')\" oncontextmenu=\"ctxMenu(event)\""
									+ "ontouchstart=\"touchManager(event,'start',this);\" ontouchend=\"touchManager(event,'end',this);\">"
                                + "</div>"
                            + "</div>"
                        + "</div>"
                    + "</div>";
		if (append=='bottom')
			bid.prepend(toape);
		else
			bid.append(toape);
    });
    redrawPoke(arr, pos, {noEnerg:true});
    if (!append||append=='bottom') {
        redrawHP(arr, pos);
		redrawEnergias(arr, pos);
	} else if (opc&&opc.nEnerg) {
		redrawEnergias([{energias:opc.nEnerg}], pos);
	}
}

function redrawPoke(dats, pos, opc) {
    if (!pos) {
        for (var p = 0; p <= 5; p++) {
            redrawPoke(dats, "b" + p + "du", {noEnerg:true});
            redrawPoke(dats, "b" + p + "op", {noEnerg:true});
        }
        return;
    }
    var arr = $("#div" + pos + " .carta_poke");
    if (!arr.length) {
		makePoke([],pos);
		return;
	}
    var tx = 0;
    var dv = parseInt($("#div" + pos).css('width'));
    var dh = parseInt($("#div" + pos).css('height'));
    var wi = dh * 0.70;
    var dx = maximo((dv - wi) / (arr.length), wi / 3);
	//var nEnerg=[];
    arr.each(function(i) {
        $(this).css({ width: wi, zIndex: arr.length - i })
		this.id="carta_"+pos+"_"+i;
        var dgl=$(this).find(".dragCarta")[0];
		var aimg=this.getAttribute('img');
		dgl.setAttribute("draginfo",pos+";"+aimg+":"+i);
		/*if (defProv[aimg]) {
			nEnerg.push({img:aimg,prov:defProv[aimg]});
		} else if (i>0&&aimg=='28') {
			nEnerg.push({img:aimg,prov:tabla.cartas[(pos.substr(2,2)=='op'?lop:ldu)][pos.substr(0,2)][0].energias.find(function(w){return w.img==aimg}).prov});
		}*/
        if (i>0) {
            dgl.onclick=null;
            dgl.ondragstart=null;
            dgl.ondragenter=null;
            dgl.ondragleave=null;
            dgl.ondragover=null;
            dgl.ondrop=null;
        }
        tx = i * dx;
        if (pos.substr(2, 2) == "op")
            $(this).animate({ right: tx });
        else
            $(this).animate({ left: tx });
    });
	/*if (!opc||!opc.noEnerg) {
		redrawEnergias([{energias:nEnerg}],pos);
	}*/
}

function movePoke(from,to,opc,cb) {
    var obj=$("#div"+from+" .carta_poke");
    var toobj=$("#div"+to+" .carta_poke");
    if (!toobj.length) {
        makePoke([],to);
        return movePoke(from,to,opc,cb);
    }
    turnIntoAnimaCarta(obj, toobj, opc, cb);
    makePoke([],from);
}

function swapPoke(from,to,opc,cb) {
    var tst=$("#div"+from+" .carta_poke")[0];
    var ttst=$("#div"+to+" .carta_poke")[0];
    if (tst&&tst.getAttribute('img')!=-1) {
        movePoke(from,to,opc,cb);
        cb=null;
    }
    if (ttst&&ttst.getAttribute('img')!=-1) {
        movePoke(to,from,opc,cb);
        cb=null;
    }
    if (cb)
        cb();
}

function redrawHP(dats, pos) {
    var arr = $("#div" + pos + " .carta_poke .overCarta");
    if (!arr.length) return;
    var over, v;
    arr.each(function(i) {
        if (i == 0) {
            v = dats[i];
            v.hp = v.hp || 0;
            v.dano = v.dano || 0;
			v.estados = v.estados || '';
            over = "";
            for (var h = 0; h < v.hp; h++) {
                over += h >= v.dano ? vidablanca : vidanegra;
            }
			for (var e=0,le='';e<v.estados.length;e++) {
				if (le!=v.estados[e])
					over += "<br>";
				le=v.estados[e];
				over += img_estados[le];
			}
        } else {
            over = "";
        }
        $(this).html(over);
    });
}

function makeDano(pos,dano,cura,opc,cb) {
    var inner="<div class='"+(cura?'cura':'dano')+"'>"+dano+"</div>";
    makeAnimaDiv(inner, $("#div"+pos+" .carta_poke"), $("#div"+pos+" .carta_poke"), {
            animDelay:10,
            translate:{x:0,y:-screen_h*0.1},
            swing:'easeOutCirc',
            fadeOut:0.3
        }, null);
    if (cb) {
        if (!opc) opc={};
        setTimeout(cb,animDelay*(opc.delayCb||3));
    }
}

function redrawEnergias(dats,pos,opc) {
	if (!dats||!dats.length||!dats[0].energias)
		return;
	var arr = $("#div" + pos + " .carta_poke .overCarta").first();
	var lo=(pos.substr(2,2)=='op');
	arr.find(".icoEnergia").remove();
	var cnt=0;
	dats[0].energias.forEach(function (v,i) {
		if (v.prov) {
			var provs=v.prov.split("");
			provs.forEach(function (w) { arr.append("<img class='icoEnergia' src='resources/images/"+w+".png' ide='"+i+"' style='top:"+(cnt*15)+"%;"+(lo?"right":"left")+":-20%;'>"); cnt++; } );
		}
	});
}

function redrawStadium(img, lo) { //tiene el make y redraw juntos
    if (img == null) {
        var cini = parseInt($("#divstadiumdu .fixInner img.cartaImgM").attr("img"));
        if (!isNaN(cini)) redrawStadium(cini);
        var cini = parseInt($("#divstadiumop .fixInner img.cartaImgM").attr("img"));
        if (!isNaN(cini)) redrawStadium(cini, true);
        return;
    }
    var sad = lo ? $("#divstadiumop") : $("#divstadiumdu");
    var dh = parseInt(sad.css('height'));
    var wi = dh * 0.60;
    sad.html("<div class='carta_stad root_carta' style='width:" + wi + "px;max-width:80%;'>" + "<div class='fixContain'>" + "<div class='fixInner'></div>" + "</div>"
        // +"<div class='label'>"+num+"x</div>"
        + "</div>"
    );
    var sadfi = sad.find(".fixInner");
    sadfi.append("<img class='cartaImgM' ondblclick='makeZoom(" + img + ")' img='" + img + "' src='" + echoCarta(img, false, lo ? lop : ldu) + "' style='top:0px;right:0px;'>");
}

function redrawSuporter(img, lo) { //tiene el make y redraw juntos
    if (img == null) {
        var cini = parseInt($("#divsuporterdu .fixInner img.cartaImgM").attr("img"));
        if (!isNaN(cini)) redrawSuporter(cini);
        var cini = parseInt($("#divsuporterop .fixInner img.cartaImgM").attr("img"));
        if (!isNaN(cini)) redrawSuporter(cini, true);
        return;
    }
    var sad = lo ? $("#divsuporterop") : $("#divsuporterdu");
    var dh = parseInt(sad.css('height'));
    var wi = dh * 0.60;
    sad.html("<div class='carta_stad root_carta' style='width:" + wi + "px;max-width:80%;'>" + "<div class='fixContain'>" + "<div class='fixInner'></div>" + "</div>"
        // +"<div class='label'>"+num+"x</div>"
        + "</div>"
    );
    var sadfi = sad.find(".fixInner");
    sadfi.append("<img class='cartaImgM'  ondblclick='makeZoom(" + img + ")' img='" + img + "' src='" + echoCarta(img, false, lo ? lop : ldu) + "' style='top:0px;right:0px;'>");
}

function redrawPrices(arr, lo) { //tiene el make y redraw juntos
    if (arr == null) {
        var cini = [];
        $("#divprizdu .fixInner img.cartaImgP").each(function() {
            var ci = parseInt($(this).attr("img"));
            if (!isNaN(ci))
                cini.push(ci);
        });
        redrawPrices(cini);
        var cini = [];
        $("#divprizop .fixInner img.cartaImgP").each(function() {
            var ci = parseInt($(this).attr("img"));
            if (!isNaN(ci))
                cini.push(ci);
        });
        redrawPrices(cini, true);
        return;
    }
    var pad = lo ? $("#divprizop") : $("#divprizdu");
    var dh = parseInt(pad.css('height'));
    var wi = dh * 0.60;
    pad.html("<div class='carta_priz root_carta' style='width:" + wi + "px;max-width:80%;" + (lo ? "left" : "right") + ":0;'>"
            + "<div class='fixContain'>"
                + "<div class='fixInner'></div>"
            + "</div>"
        + "</div>"
    );
    var padfi = pad.find(".fixInner");
    var miti = Math.ceil(minimo(arr.length, 6) / 2);
    var topi = 90 / miti;
    for (var p = 0; p < arr.length; p++) {
        if (arr[p] == null || typeof arr[p] == 'undefined')
            arr[p] = -1;
        if (arr[p] != -1 || arr.length <= 6)
            padfi.append("<img class='cartaImgP' id='priz"+p+"' onclick='select(event)' draginfo='p;" + arr[p] + ":" + p + "' ondblclick='makeZoom(" + arr[p] + ")' img='" + arr[p] + "' src='" + echoCarta(arr[p], false, lo ? lop : ldu) + "' style='cursor:pointer;top:" + ((p % miti) * topi) + "%;" + (lo ? "left" : "right") + ":" + (Math.floor(p / miti) * 50) + "%;z-index:" + p + ";'>");
    }
}

function ponerPrices(arr, lo, cb) {
    var cini = parseInt($("#divmazo" + (lo ? "op" : "du") + " div.label").text());
    for (var c = 0; c < arr.length; c++) {
        var cc = {
            c: c + 1,
            tarr: arr.filter((v, i) => i <= c),
            cb: function() {
                redrawPrices(this.tarr, lo);
                if (this.c == arr.length && cb) cb();
            }
        };
        if (lo)
            var fu = function() {
                redrawMazo(cini - this.c, lo);
                makeAnimaCarta(echoCarta(0, false, lo), $("#divmazoop .fixInner"), $("#divprizop .fixInner"), { efectoResize: 0.3 }, this.cb.bind(this))
            };
        else
            var fu = function() {
                redrawMazo(cini - this.c, lo);
                makeAnimaCarta(echoCarta(0, false, lo), $("#divmazodu .fixInner"), $("#divprizdu .fixInner"), { efectoResize: 0.3 }, this.cb.bind(this))
            };
        setTimeout(fu.bind(cc), c * animDelay);
    }
}

function robarPrice(ind, lo, cb) {
    if (lo) {
        var priz=$("#divprizop #priz"+ind);
        makeAnimaCarta(priz.attr("src"), priz, posPlayerOp, {animDelay:3}, cb)
        priz.attr("img",-1);
        redrawPrices();
    } else {
        var priz=$("#divprizdu #priz"+ind);
        makeAnimaCarta(priz.attr("src"), priz, posPlayerDu, {animDelay:3}, cb)
        priz.attr("img",-1);
        redrawPrices();
    }
}


function redrawMazo(num, lo) { //tiene el make y redraw juntos
    if (num == null) {
        var cini = parseInt($("#divmazodu div.label").text());
        if (isNaN(cini)) cini = 0;
        redrawMazo(cini);
        cini = parseInt($("#divmazoop div.label").text());
        if (isNaN(cini)) cini = 0;
        redrawMazo(cini, true);
        return;
    }
    var maz = lo ? $("#divmazoop") : $("#divmazodu");
    var dh = parseInt(maz.css('height'));
    var wi = dh * 0.60;
    maz.html("<div class='carta_mazo root_carta' style='width:" + wi + "px;max-width:80%;'>" + "<div class='fixContain'>" + "<div class='fixInner'></div>" + "</div>" + "<div class='label'>" + num + "x</div>" + "</div>");
    var mazfi = maz.find(".fixInner");
    if (num == 0) {
        mazfi.append("<img class='cartaImgM' src='" + echoCarta(0, false, lo ? lop : ldu) + "' style='opacity:0.1;bottom:0px;right:0px;'>");
    } else {
        for (var l = 0; l < num; l += 5) {
            var ll = parseInt(0.2 * l) + "%";
            mazfi.append("<img class='cartaImgM' src='" + echoCarta(0, false, lo ? lop : ldu) + "' style='bottom:" + ll + ";right:" + ll + ";'>");
        }
    }
}

function manoAlDeck(lo, cb) {
	if (lo) {
		var last=$("#divmanoop .cartaImgH").length-1;
		$("#divmanoop .cartaImgH").each(function(i,v){
			makeAnimaCarta(echoCarta(0,false,lop),posPlayerOp, $("#divmazoop"), {/*efectoResize:1,*/preAnimWait:i*0.5}, function(){
				if(i==last&&cb) {
					cb();
				}
			});
		});
	} else {
		var last=$("#divmanoduhid .carta_mano").length-1;
		$("#divmanoduhid .carta_mano").each(function(i,v){
			turnIntoAnimaCarta($(this), $("#divmazodu"), {/*efectoResize:1,*/preAnimWait:i*0.5}, function(){
				if(i==last&&cb) {
					cb();
				}
			});
		});
	}
}

function barajarMazo(lo, cb) {
    var maz = $("#divmazo" + (lo ? 'op' : 'du'));
    var cini = parseInt(maz.find("div.label").text());
    if (isNaN(cini)) cini = 0;
    if (cini == 0) {
        if (cb) cb();
        return;
    }
    var mazfi = maz.find(".fixInner");
    for (var l = 0; l < maximo(5, cini - 1); l += 1) {
        var ll = parseInt(0 * l) + "%";
        mazfi.prepend("<img class='cartaImgM aRemover' src='" + echoCarta(0, false, lo ? lop : ldu) + "' style='bottom:" + ll + ";right:" + ll + ";'>");
    }
    maz.find("img.cartaImgM").each(function(i, v) {
        var aho = $(this).css(['right', 'bottom']);
        $(this).animate({ right: (-25 + Math.random() * 50) + '%', bottom: (-25 + Math.random() * 50) + '%' }, animDelay * 2, 'linear')
            .animate({ right: (-25 + Math.random() * 50) + '%', bottom: (-25 + Math.random() * 50) + '%' }, animDelay * 2, 'linear')
            .animate({ right: (-25 + Math.random() * 50) + '%', bottom: (-25 + Math.random() * 50) + '%' }, animDelay * 2, 'linear')
            .animate(aho, animDelay * 2, 'linear', function() {
                if ($(this).hasClass("aRemover")) $(this).remove();
                if (i == 0 && cb) cb()
            });
    });
}

function shakePos(pos, modo, opc, cb) {
	var lo=pos.indexOf('op')>0;
	var sele=$("#div"+pos+" .root_carta").first();
	opc=opc||{};
	if (!modo||modo=='shake') {
		opc.count=opc.count||5;
		opc.distX=opc.distX||1;
		opc.distY=opc.distY||1;
		for (var i=0;i<opc.count;i++) {
			if (lo) sele.animate({right:((rand(-20,20)/10)*opc.distX)+'%',top:((rand(-20,20)/10)*opc.distY)+'%'},animDelay,opc.easing||'easeOutCirc',null);
			else sele.animate({left:((rand(-20,20)/10)*opc.distX)+'%',top:((rand(-20,20)/10)*opc.distY)+'%'},animDelay,opc.easing||'easeOutCirc',null);
		}
	} else if (modo=='wipe') {
		opc.distY=opc.distY||2;
		opc.easing=opc.easing||'easeInQuint';
		if (lo) sele.animate({top:(5*opc.distY)+'%'},animDelay*3,opc.easing||'easeOutCirc',null);
		else sele.animate({top:(-5*opc.distY)+'%'},animDelay*3,opc.easing||'easeOutCirc',null);
		opc.easing='linear';
	}
	if (lo) sele.animate({right:0,top:0},animDelay,opc.easing||'easeOutCirc',cb);
	else sele.animate({left:0,top:0},animDelay,opc.easing||'easeOutCirc',cb);
}

function robarCartas(cant, lo, cb, stpcb) {
    var cini = parseInt($("#divmazo" + (lo ? "op" : "du") + " div.label").text());
    for (var c = 0; c < cant; c++) {
        var cc = { c: c + 1 };
        if (cb && stpcb) {
            var ncb = function() {
                stpcb(this.c);
                cb();
            }.bind(cc);
        } else {
            var ncb = cb;
        }
        if (lo)
            var fu = function() {
                redrawMazo(cini - this.c, lo);
                makeAnimaCarta(echoCarta(0, false, lo), $("#divmazoop .fixInner"), posPlayerOp, {}, this.c == cant ? ncb : function() {
                    if (stpcb) stpcb(this.c)
                }.bind(this))
            };
        else
            var fu = function() {
                redrawMazo(cini - this.c, lo);
                makeAnimaCarta(echoCarta(0, false, lo), $("#divmazodu .fixInner"), posPlayerDu, {}, this.c == cant ? ncb : function() {
                    if (stpcb) stpcb(this.c)
                }.bind(this))
            };
        setTimeout(fu.bind(cc), c * animDelay * 2);
    }
}

function iniMazo(cant, lo, cb) {
    for (var c = 0, st = 5; c < cant; c += st) {
        var cc = { c: c + st };
        cc.c = maximo(cc.c, cant);
        if (lo)
            var fu = function() {
                makeAnimaCarta(echoCarta(0, false, true), posPlayerOp, $("#divmazoop .fixInner"), {}, function() {
                    redrawMazo(this.c, true);
                    if (cb && this.c >= cant) cb()
                }.bind(this))
            };
        else
            var fu = function() {
                makeAnimaCarta(echoCarta(0, false, false), posPlayerDu, $("#divmazodu .fixInner"), {}, function() {
                    redrawMazo(this.c, false);
                    if (cb && this.c >= cant) cb()
                }.bind(this))
            };
        setTimeout(fu.bind(cc), c * animDelay / st);
    }
}

function redrawDiscard(arr, lo, append) { //tiene el make y redraw juntos
    var des = lo ? $("#divdescop") : $("#divdescdu");
    if (arr == null) {
        var narr = [];
        des.find(".cartaImgD").each(function(i) {
            var cim = $(this).attr("img");
            if (cim != -1) narr.push(cim);
        });
        redrawDiscard(narr, lo, false);
        if (!lo&&lo==null) redrawDiscard(null, !lo, false);
        return;
    } else if (arr == 'label') {
		var des = lo ? $("#divdescop") : $("#divdescdu");
		var desfi = des.find(".fixInner");
		des.find("div.label").text((desfi.find(".cartaImgD").length) + "x");
		return;
	}
    var dw = parseInt(des.css('width'));
    var dh = parseInt(des.css('height'));
    var wi = dh * 0.60;
    if (!append) des.html("<div class='carta_desc root_carta' style='width:" + wi + "px;max-width:90%;'>" + "<div class='fixContain'>" + "<div class='fixInner'></div>" + "</div>" + "<div class='label'>" + arr.length + "x</div>" + "</div>");
    var desfi = des.find(".fixInner");
    if (!append && arr.length == 0) {
        var dw = parseInt(desfi.css('width'));
        var dh = parseInt(desfi.css('height'));
        desfi.append("<img class='cartaImgD' img='-1' src='" + echoCarta(-1, false, lo ? lop : ldu) + "' style='right:" + Math.round(dw / 2 - dw * 0.2) + "px;bottom:" + Math.round(dh / 2 - dw * 0.2) + "px'>");
    } else {
        for (var l = 0; l < arr.length; l += 1) {
            desfi.append("<img class='cartaImgD csf' img='" + arr[l] + "' src='" + echoCarta(arr[l], false, lo ? lop : ldu) + "' style='right:" + rand(dw * 0, dw * 0.4) + "px;bottom:" + rand(dw * 0, dh * 0.3) + "px;transform:rotate(" + rand(0, 359) + "deg);-webkit-transform:rotate(" + rand(0, 359) + "deg);'>");
        }
        var cini = parseInt(des.find("div.label").text());
        des.find("div.label").text((desfi.find(".cartaImgD").length) + "x");
    }
}

function makeDescartar(arr,pos,opc,cb) {
    var lo=pos.substr(-2)=='op';
    var divi=$("#div"+pos);
    for (var i=0;i<arr.length;i++) {
        var cc={c:arr[i],i:i};
        if (i+1==arr.length)
            cc.fu=function () { redrawDiscard([this.c], lo, true); if(cb)cb(); };
        else
            cc.fu=function () { redrawDiscard([this.c], lo, true); };
        setTimeout(function () {
            var todi=divi.find("[img="+this.c+"]");
            if (todi.length) {
                turnIntoAnimaCarta(todi.first(), $("#divdesc"+pos.substr(-2)), {efectoResize:0.4}, this.fu.bind(this));
            }
        }.bind(cc),i*animDelay);
    }
}

function redrawPlayer(dats, lo) {
    var divi = $("#divplayer" + (lo ? "op" : "du"));
    divi.find(".playername").text(dats.cuenta);
    divi.find(".playerimg").css("background-image", "url(" + rutaimg + '../avatares/' + dats.imagen + ")");
}

function redrawTiempo() {
	$(".ladoturno").removeClass("ladoturno");
    if (tabla && tabla.tiempo && tabla.now && tabla.desde && tabla.hasta && tabla.tiempode != -1) {
        if (tabla.tiempode == lop || tabla.tiempode == ldu) {
            var divi = $("#divplayer" + (tabla.tiempode == lop ? "op" : "du")).addClass("ladoturno");
            var tiempoT = Math.floor((tabla.hasta - tabla.now) / 1000);
            var tiempoG = Math.floor(tabla.tiempo[tabla.tiempode] / 1000);
            if (tiempoT < 0) { tiempoG += tiempoT; /*tabla.tiempo[tabla.tiempode]=tiempoG;*/ }
            tiempoG = minimo(tiempoG, 0);
            tiempoT = minimo(tiempoT, 0);
            divi.find(".relojtot").text(enReloj(tiempoG));
            divi.find(".relojtur").text(enReloj(tiempoT));
        }
    } else {
		//console.log(tabla.tiempo,tabla.now,tabla.desde,tabla.hasta,tabla.tiempode)
	}

}

sit = setInterval(function() {
    redrawTiempo();
	if (!animCtl.isAnimando())
        checkEtapa();
    tabla.now += 1000;
}, 1000);
//evs
function keypress(e) {
    var preventEscape = false;
    if (!kbfocus) {
        if (e.key == ' ') {
            $("#divmanoductl").trigger("mouseenter");
            e.preventDefault();
        }
    } else if (kbfocus == 'mano') {
        if (e.key == 'Escape') {
            $("#divmanoductl").trigger("mouseleave");
            e.preventDefault();
            preventEscape = true;
        } else if (e.key == ' ') {
            $("#divmanoductl").trigger("mouseleave");
            e.preventDefault();
        }
        /* else if (e.key=='ArrowRight') {
        			var seles=$("#divmanoductl .carta_mano");
        			var sl=seles.find(".selected");
        			if (sl.length==0) {
        				seles.first().addClass("selected");
        			} else if (sl.length>=2) {
        				sl.removeClass("selected");
        				seles.first().addClass("selected");
        			}
        		}*/
    } else if (kbfocus == 'elecciones') {
        if (e.key == 'Escape') {
            $("#divelecciones")[0].onperform("_cancel");
            e.preventDefault();
            preventEscape = true;
        }
    }
    //en cualquier parte
    if (e.key == 'Escape') {
        if ($(".focused,.selected").length > 0) {
            select();
            $(".focused").removeClass("focused");
            preventEscape = true;
        }
        /*if (!preventEscape) {
            if (!$("#divtodobatalla").hasClass("conchat"))
                divchatlogs.fadeToggle(animDelay * 2);
        }*/
        e.preventDefault();
    } else if (e.key == 'F2') {
        cambiarTodo(!divtodobatalla.hasClass("conchat"), null);
        e.preventDefault();
    } else if (e.key == 'F3') {
        cambiarTodo(null, !divtodobatalla.hasClass("conmano"));
        e.preventDefault();
    } else {
        console.log(e);
    }
}

function select(e) {
    $(".dragover").removeClass("dragover");
    if (!e) {
        $(".selected").removeClass("selected");
        return;
    }
    if ($(e.target).hasClass("selected")) {
        $(e.target).removeClass("selected");
    } else {
        //var seles = $("#divbatalla .selected");
        var seles = $(".selected");
        if (nidselect == 0 && seles.length >= 1) {
            var dg = seles[0].getAttribute("draginfo");
            if (!dg)
                dg = $(seles[0]).find(".dragCarta")[0].getAttribute("draginfo");
            var dp = e.target.getAttribute("draginfo");
            if (!dp)
                dp = $(e.target).find(".dragCarta")[0].getAttribute("draginfo");
            lastCartaUsada = seles.first().parents(".root_carta").first();
            seles.removeClass("selected");
            interpretarAccion("click", dg, dp);
        } else {
            $(e.target).addClass("selected");
        }
    }
    e.stopPropagation();
}

function ctxMenu(e) {
    var arr=[];
    var tosend=[];
    var sele=$(e.target);

	arr.push("Ver cartas");
    tosend.push("--view-pos");
    if (sele.attr('draginfo').split(';')[0]=='b0du') {
        arr.push("Atacar");
        tosend.push("getAtaques");
    }
    if (sele.attr('draginfo').split(';')[0].substr(2,2)=='du') {
		var carta=parseInt(sele.attr('draginfo').split(';')[1].split(':')[0]);
		var pkpw=arr_pkpw.find(function(v){return v.carta==carta});
		if (pkpw) {
			arr.push(pkpw.nombre);
			tosend.push("pokepower "+sele.attr('draginfo').split(';')[0].substr(0,2)+" "+pkpw.pkpw);
		}
    }
	arr.push("Pasar Turno");
    tosend.push("pasarTurno");
    if (arr.length>0) {
        showElecciones("botones", arr, tosend, {needToPerform:1,dontFadeOut:true}, (ee,v)=>{if (v=='--view-pos') {ee.preventDefault();showCartasDePos(sele.parents(".root_carta")[0].parentNode,{texto:"Cartas:"});}else{showElecciones();}});
    }
    e.stopPropagation();
    e.preventDefault();
}

function allowDrop(e) {
    e.preventDefault();
}

function drag(e) {
    $(".dragover").removeClass("dragover");
    var dg = e.target.getAttribute("draginfo");
    e.dataTransfer.setData("text", dg);
	var dimg=$(e.target).attr("dragimg");
	//alert(dimg);
    if (dimg) {
        var img = document.createElement("img");
        img.src = dimg;
        e.dataTransfer.setDragImage(img, img.width / 2, img.height / 2);
    }
    lastCartaUsada = $(e.target).parents(".root_carta").first();
    if (dg.split(";")[0] == "mano")
        $("#divmanoductl").trigger("mouseleave");
}

function drop(e) {
    e.preventDefault();
    var dg = e.dataTransfer.getData("text");
    var dp = e.target.getAttribute("draginfo");
    if (!dp)
        dp = $(e.target).find(".dragCarta")[0].getAttribute("draginfo");
    e.stopPropagation();
    $(".dragover").removeClass("dragover");
    interpretarAccion("drop", dg, dp);
}

function touchManager(ev,mode,tgt) {
	if (!touchCtl)
		touchCtl={dp:'',tp:'',dragimg:'',w:0,h:0,lastClick:Date.now(),
			animacarta:$(document.createElement('div'))
				.appendTo(divtodobatalla)
				.addClass("animaDiv")
				.css({display:'none',opacity:0.5,width:'15%',height:'15%'})
		};
	//ev.stopPropagation();
	//ev.preventDefault();
	if (mode=='start'&&tgt) {
		lastCartaUsada = $(tgt).parents(".root_carta").first();
		touchCtl.dp=tgt.getAttribute("draginfo");
		touchCtl.lastClick=Date.now();
		touchCtl.dragimg=tgt.getAttribute("dragimg");
		touchCtl.w=touchCtl.animacarta.width();
		touchCtl.h=touchCtl.animacarta.height();
	} else if (mode=='end'&&!tgt&&!ev.touches.length) {
		var dondeva=$(".root_carta.carta_poke");
		var dx=ev.changedTouches[0].pageX;
		var dy=ev.changedTouches[0].pageY;
		var sq,tx0,w,tx1,ty0,h,ty1,diana=[];
		dondeva.each(function (i,v) {
			sq=$(v).offset();
			tx0=sq.left;
			w=$(v).width();
			tx1=sq.left+w;
			ty0=sq.top;
			h=$(v).height();
			ty1=sq.top+h;
			if (tx0<dx&&tx1>dx&&ty0<dy&&ty1>dy) {
				diana.push({dist:Math.sqrt(Math.pow(tx0+w/2-dx,2)+Math.pow(ty0+h/2-dy,2)),draginfo:$(v).find(".dragCarta")[0].getAttribute("draginfo")});
			}
		});
		if (!diana.length) {
			//que hacer cuando no encuentre nada
		} else {
			diana=diana.sort((a,b)=>(a.dist-b.dist));
			touchCtl.tp=diana[0].draginfo;
			if (touchCtl.dp!=''&&touchCtl.dp!=touchCtl.tp)
				interpretarAccion('touch', touchCtl.dp, touchCtl.tp, {});
		}
		touchCtl.dp='';
		touchCtl.tp='';
		touchCtl.dragimg='';
		touchManager(ev,'move',null);
	} else if (mode=='move') {
		if (touchCtl.dragimg=='')
			touchCtl.animacarta.css({display:'none'});
		else if (ev.touches.length) {
			if (touchCtl.animacarta.css('display')=='none') {
				touchCtl.animacarta.css({display:'block',backgroundImage:'url('+touchCtl.dragimg+')'});
			}
			touchCtl.animacarta.css({left:ev.touches[0].pageX-touchCtl.w/2,top:ev.touches[0].pageY-touchCtl.h/2});
			lastclick=Date.now();
		}
	}
}

function interpretarAccion(tipo, fr, to, opc) {
    showElecciones();
    var frs = fr.split(";");
    var tos = to.split(";");
    if (frs[0] == "mano") {
        if (["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(tos[0].substr(0, 2)) != -1) {
            enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
        }
    } else if (frs[0] == "responde") {
        enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
    } else if (frs[0] == "b0du" && tos[0] == "b0op") { //proximo a eliminarse
        enviarDatos("accmesa getAtaques");
    } else if (frs[0] != tos[0] && ["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(frs[0].substr(0, 2)) != -1 && ["b0", "b1", "b2", "b3", "b4", "b5"].indexOf(tos[0].substr(0, 2)) != -1) {
        enviarDatos("accmesa " + frs.join(" ") + " " + tos.join(" "));
    }
	clickdown=0;
}
/*draws generales*/
function makeAnimaDiv(inner, cssfr, cssto, opc, cb) {
    if (!opc) opc={};
    if (cssfr instanceof jQuery) {
        cssfr = cssfr.first();
        var ncssfr = cssfr.css(["width", "height"]);
        var fof = cssfr.offset();
        ncssfr.top = fof.top;
        ncssfr.left = fof.left;
        cssfr = ncssfr;
    }
    if (cssto instanceof jQuery) {
        cssto = cssto.first();
        var ncssto = cssto.css(["width", "height"]);
        var tof = cssto.offset();
        ncssto.top = tof.top;
        ncssto.left = tof.left;
        cssto = ncssto;
    }
    if (cssto.width == -1) cssto.width = cssfr.width;
    if (cssto.height == -1) cssto.height = cssfr.height;

    if (opc.efectoResize) {
        var inW = parseInt(cssto.width);
        var inH = parseInt(cssto.height);
        cssto.width = inW * opc.efectoResize;
        cssto.height = inH * opc.efectoResize;
        cssto.left += (inW - parseInt(cssto.width)) / 2;
        cssto.top += (inH - parseInt(cssto.height)) / 2;
    }
    if (opc.translate) {
        cssto.left+=opc.translate.x;
        cssto.top+=opc.translate.y;
    }

    ret=$(document.createElement('div'))
        .appendTo(divtodobatalla)
        .addClass("animaDiv")
		.addClass(opc.reClass||'')
		.html(inner)
        .css(cssfr)
		.delay(animDelay *(opc.preAnimWait||0))
        .animate(cssto, animDelay * (opc.animDelay||3), opc.swing||"linear", function() {
			$(this).css({borderSpacing:0})
			.animate({borderSpacing:100},animDelay * (opc.animWait||0),'linear',function () {
				/*if (opc.reClass) {
					$(this).addClass(opc.reClass);
				}//*/
	            if (!opc.dontRemove) {
					$(this).remove();
				}
				if (opc.cbDelayed) {
					cbDelayed(this,opc);
				}
				if (cb) cb();
			})
        });
    if (opc.fadeOut) {
        var wait=animDelay*(opc.animDelay||3)*(opc.fadeOut||0.5);
        var saldo=animDelay*(opc.animDelay||3)-wait;
        ret.children().css({opacity:1}).delay(wait).animate({opacity:0},saldo);
    }
	return ret;
}

function makeAnimaCarta(img, cssfr, cssto, opc, cb) {
    makeAnimaDiv(" ", cssfr, cssto, opc, cb)
        .css({ backgroundImage: "url(" + img + ")" });
}

function turnIntoAnimaCarta(obj, toobj, opc, cb) {
	if (!obj.length) {
		obj=$(obj.selector);
	}
    //console.log(obj);
    if (obj.hasClass("cartaImgP")||obj.hasClass("cartaImgD"))
        var img=obj[0].src;
	else if (obj.hasClass("animaDiv"))
        var img=obj.css("backgroundImage").split('"').join('').split("'").join('').split("url(").join('').split(")").join('');
    else
        var img = obj.find("img.cartaImg")[0]?obj.find("img.cartaImg")[0].src:obj.css("backgroundImage");
    var cssfr = obj.css(["width", "height"]);
    var fof = obj.offset();
	if (!fof)
		console.log("error fof",obj,fof)
    cssfr.top = fof.top;
    cssfr.left = fof.left;
    if (toobj instanceof jQuery) {
        var cssto = toobj.css(["width", "height"]);
        var tof = toobj.offset();
        cssto.top = tof.top;
        cssto.left = tof.left;
    } else {
        var cssto=toobj;
    }
    obj.remove();
    //alert(JSON.stringify(img));
    makeAnimaCarta(img, cssfr, cssto, opc, cb);
}

function turnIntoCartaFlip(obj, toimg, opc, cb) {
    var tst = obj.find("img.cartaImg");
    if (tst.length == 0) {
        if (cb) cb();
        return;
    }
    var img = obj.find("img.cartaImg")[0].src || obj.css("backgroundImage");
    var pos = obj.parents(".activo,.poke")[0].id.substr(3);
    var cssfr = obj.css(["width", "height"]);
    var fof = obj.offset();
    cssfr.top = fof.top;
    cssfr.left = fof.left;
    obj.remove();
    makePoke([{img:-1}],pos);
    var ret=$(document.createElement('div'))
        .appendTo(divtodobatalla)
        .addClass("animaDiv")
        .css(cssfr)
        .css({ transform_rotateY: 0, backgroundImage: 'url(' + img + '), url(' + toimg + ')' })
        .animate({ transform_rotateY: 90 }, {
            duration: animDelay * 2,
            step: function(now, tw) { tw.elem.style.transform = "rotateY(" + (now) + "deg)" },
            easing: 'linear'
        })
        .animate({ transform_rotateY: 90 }, {
            duration: animDelay * 2,
            start: function(prom) { prom.elem.style.backgroundImage = 'url(' + toimg + ')' },
            step: function(now, tw) { tw.elem.style.transform = "rotateY(" + (now - 90) + "deg)" },
            easing: 'linear',
            complete: function() {
                if (cb) cb();
                $(this).remove()
            }
        })
        .parent().css({ perspective: '300px' });
    return ret;
}

function showCartasDePos(elem,opc) {
	opc=opc||{};
    var toshow=[];
    var sele=$(elem).find(".fixInner .csf").each((i,v)=>{
        toshow.push($(v).attr('img'));
    });
    showElecciones("cartas", toshow, null, {texto:opc.texto||"Descarte:"}, v=>{v.preventSend=true;});
}

function showError(inner,opc,cb) {
    if (!opc) opc={};
    var ret=$(document.createElement('div'))
        .appendTo($("#divbatalla"))
        .addClass("errorDiv")
        .html(inner)
        .animate({opacity:0},animDelay*25,opc.swing||'easeInExpo',function () {
                if (cb) cb();
                $(this).remove();
            }
        );
    return ret;
}

function showGanador(dats,opc,cb) {
    if (!opc) opc={};
    if (dats.ganador==-1) {
        var resultado="EMPATE";
        var ganador="-";
    } else {
        var resultado="VICTORIA";
        var ganador=tabla.players[dats.ganador].cuenta;
    }
    var ret=$(document.createElement('div'))
        .appendTo($("#divbatalla"))
        .addClass("congratzDiv")
        .html("<div class='cont'>&nbsp;</div><div class='cont'>"+resultado+"<div class='ganador'>"+ganador+"</div></div><div class='cont'>&nbsp;</div>")
        .delay(opc.tiempo||15000)
        .animate({opacity:0},animDelay*5,opc.swing||'easeInExpo',function () {
                if (cb) cb();
                $(this).remove();
            }
        );
    ret.find(".ganador").fadeIn(animDelay*8);
    return ret;
}

function showElecciones(tipo, arr, tosend, opc, cb) {
    var divele = $("#divelecciones");
    if (!tipo) {
        divele.fadeOut(animDelay);
        kbfocus = null;
        return;
    } else if (tabla&&tabla.etapa==3) {
        return;
    }
    if (!opc) opc = {};
    if (!arr) arr = [];
    var texti = divele.find("#diveleccionestexto").html(opc.texto || '');
    var divi = divele.find("#diveleccionesinner").html("");
    divele.css({"clip-path":"initial","-webkit-clip-path":"initial"});
    divele.fadeIn(animDelay * 3);
    if (tipo == "botones") {
        arr.forEach(function(v, i) {
            $("<div class='botones_vertical' tosend='" + (tosend ? tosend[i] : v) + "'>" + v + "</div>").appendTo(divi)
                .on("click", function(ev) {
                    ev.stopPropagation();
					ev.preventDefault();
                    $(this).parents("#divelecciones")[0].onperform(tosend ? tosend[i] : v, this);
                });
        });
    } else if (tipo == "ataques") {
        arr.forEach(function(v, i) {
            $("<div class='botones_vertical ataques' tosend='" + (tosend ? tosend[i] : v) + "'>" + v + "</div>").appendTo(divi)
                .on("click", function(ev) {
                    ev.stopPropagation();
					ev.preventDefault();
                    $(this).parents("#divelecciones")[0].onperform(tosend ? tosend[i] : v, this);
                });
        });
    } else if (tipo == "imagenes") {
        arr.forEach(function(v, i) {
            $("<img class='botones_inline' tosend='" + (tosend ? tosend[i] : v) + "' src='" + v + "'/> ").appendTo(divi)
                .on("click", function(ev) {
                    ev.stopPropagation();
					ev.preventDefault();
                    $(this).parents("#divelecciones")[0].onperform(tosend ? tosend[i] : v, this);
                })
                .on("dblclick", function(ev) {
                    makeZoom($(this).attr('tosend').split(":")[0]);
					ev.preventDefault();
                })
                .on("contextmenu", function(ev) {
                    $(this).dblclick();
                    ev.preventDefault();
                    ev.stopPropagation();
                });
        });
		if (opc&&opc.otrasCartas) {
			opc.otrasCartas.forEach(function(v, i) {
	            $("<img class='inactivo_inline' tosend='-' src='" + v + "'/> ").appendTo(divi);
			});
		}
    } else if (tipo == "cartas") {
        tosend = arr.map((v, i) => v + ':' + i);
        arr = arr.map(v => echoCarta(v, false, opc.lado || ldu));
		if (opc&&opc.otrasCartas) {
			opc.otrasCartas=opc.otrasCartas.map(v => echoCarta(v, false, opc.lado || ldu));
		}
        showElecciones("imagenes", arr, tosend, opc, cb);
        return;
    } else if (tipo=='prices') {
        if (opc.lado==lop)
            divele.css({"clip-path":"url(#svg_pricesop)","-webkit-clip-path":"url(#svg_pricesop)"});
        else
            divele.css({"clip-path":"url(#svg_pricesdu)","-webkit-clip-path":"url(#svg_pricesdu)"});
        $(".selected").removeClass("selected");
        $("#divselectresponde").addClass("selected");
        //return;
    } else if (tipo=='pokemon') {
        if (opc.lugares=='solo_banca') {
            if (opc.lado==lop)
                divele.css({"clip-path":"url(#svg_bancaop)","-webkit-clip-path":"url(#svg_bancaop)"});
            else
                divele.css({"clip-path":"url(#svg_bancadu)","-webkit-clip-path":"url(#svg_bancadu)"});
        } else {
            if (opc.lado==lop)
                divele.css({"clip-path":"url(#svg_bancaAop)","-webkit-clip-path":"url(#svg_bancaAop)"});
            else
                divele.css({"clip-path":"url(#svg_bancaAdu)","-webkit-clip-path":"url(#svg_bancaAdu)"});
        }
		if (!opc.movimiento) {
	        $(".selected").removeClass("selected");
	        $("#divselectresponde").addClass("selected");
		}
        //return;
    } else if (tipo == "moneda") {
		var tirasola=autocoin||opc.lado==lop;
		divi.css({overflow:'visible'});
        var moneda=$("<div id='coin_anim_container'><div id='coin_anim' class='flipping'><div class='front face'><img src='"+rutaimg+"../coins/compile/0_O.png'></div><div class='back face center'><img src='"+rutaimg+"../coins/compile/0_X.png'></div></div></div>")
			.appendTo(divi);
		var lanzar=$("<button class='lanzador'>Lanzar</button>")
			.appendTo(divi);
		var autom=$("<div id='checkautom'><input type='checkbox' "+(tirasola?'checked':'')+" "+(opc.lado==lop?'disabled':'')+"> Lanzar automáticamente</div>")
			.appendTo(divi);
		moneda.onflip=false;
		moneda.lanzamiento=function () {
			lanzar.attr('disabled',true);
			if (!this.onflip) {
				this.onflip=true;
				this.delay(animDelay*5)
					.animate({top:'-100%'},animDelay*5,'easeOutCirc')
					.animate({top:'0%'},animDelay*5,'easeOutBounce',function(){
						$(this).find("#coin_anim").removeClass("flipping").addClass(opc.cara?"head":"tail");
						if (opc.appendToLog) {
							opc.appendToLog.append("<img src='"+rutaimg+"../coins/compile/0_"+(opc.cara?'O':'X')+".png'>");
						}
					})
					.delay(animDelay*5)
					.queue(function(next) {
						showElecciones();
						if (cb) {
							cb();
						}
						next();
					});
			}
		}
		lanzar.on("click",function(){moneda.lanzamiento();});
		autom.find('input').on('change',function () {
			if (this.checked) {
				autocoin=true;
				moneda.lanzamiento();
			} else {
				autocoin=false;
			}
		});
		if (tirasola) {
			moneda.lanzamiento();
		}
		divele.find("#diveleccionescancel").hide();
		divele.find("#diveleccionesdone").hide();
    }
    //the draw
    var dw = parseInt(divi.css("width"));
    var dh = parseInt(divi.css("height"));
    var dwe = parseInt(divi.parent().css("width"));
    var dhe = parseInt(divi.parent().css("height"));
    var dwt = parseInt(texti.css("width"));
    var dht = parseInt(texti.css("height"));
    divi.css({ top: (dhe - dh) / 2, left: (dwe - dw) / 2 });
    texti.css({ top: (dhe - dht) / 2, left: 5 });
	if (tipo=='moneda') {
		return;
	} else if (opc.mustBeConfirmed) {
        divele.find("#diveleccionesdone").show();
    } else if (opc.notIncomplete) {
        divele.find("#diveleccionesdone").hide();
    } else {
        divele.find("#diveleccionesdone").show();
    }
    //the perform
    divele[0].needToPerform = opc.needToPerform;
	if (!divele[0].needToPerform&&divele[0].needToPerform!==0)
		divele[0].needToPerform=1;
    divele[0].multiplePerform = [];
    divele[0].prePerform = opc.prePerform || [];
    divele[0].postPerform = opc.postPerform || [];
    divele[0].mustBeConfirmed = opc.mustBeConfirmed || false;
    //divele[0].needToPerform-=divele[0].prePerform.length+divele[0].postPerform.length;
    if (divele[0].needToPerform > arr.length || opc.modoPerform=='remocion') {
        divele[0].postPerform.push("_complete");
        divele[0].needToPerform = arr.length;
		divele[0].mustBeConfirmed = false;
    }
    divele[0].onperform = function(val, caller) {
        this.multiplePerform.push(val);
        if (caller) {
            if (opc.modoPerform == 'seleccion') {
                var testi = this.multiplePerform.indexOf(val);
                if (testi != -1 && testi != this.multiplePerform.length - 1) {
                    this.multiplePerform.splice(testi, 1);
                    $(caller).parent().children("[tosend='" + val + "']").first().removeClass("selected");
                    this.multiplePerform.pop();
                } else {
                    $(caller).addClass("selected");
                }
            } else if (opc.modoPerform == 'remocion') {
                $(caller).off("click").slideUp(animDelay);

            }
        }
        if ((this.multiplePerform.length >= this.needToPerform && !this.mustBeConfirmed) || !val || val == '_complete' || val == '_cancel') {
            //else if (val=='_complete') divele[0].multiplePerform.pop();
            if (this.mustBeConfirmed && val != '_cancel' && (val != '_complete' || (this.needToPerform!==0&&this.multiplePerform.length > this.needToPerform + 1))) {
                alert("Debes elegir " + this.needToPerform + " opcion(es).");
                if (val == '_complete')
                    this.multiplePerform.pop();
                return;
            }

            if (!val || val == '_cancel') this.multiplePerform = ["_cancel"];
            this.multiplePerform = this.prePerform.concat(this.multiplePerform);
            if (val == '_complete' && this.postPerform[this.postPerform.length - 1] == '_complete')
                this.postPerform.pop();
			if (!opc.dontFadeOut)
				showElecciones();
            this.multiplePerform = this.multiplePerform.concat(this.postPerform);
            var hook = { preventSend: false, preventDefault: function() { this.preventSend = true } };
            if (cb) cb(hook, val, this.multiplePerform);
            if (!hook.preventSend) {
                enviarDatos("accmesa " + (this.multiplePerform.join(" ") || null));
			}
        }
    }
    kbfocus = 'elecciones';
}

function doResize() {
    screen_w = divtodobatalla[0].offsetWidth;
    screen_h = divtodobatalla[0].offsetHeight;
    screen_ver = screen_h > screen_w;
    cambiarTodo(opc_chat_disp, opc_mano_disp, screen_ver);
}
$(window).resize(function() {
    doResize();
});
$(document).ready(function() {
	document.body.onmousedown = function() { clickdown=1; lastclick=Date.now(); }
	document.body.onmouseup = function() { clickdown=0; lastclick=Date.now(); }
	document.body.ontouchstart = function(e) { if (e.touches.length==1) clickdown=1; lastclick=Date.now(); touchManager(e,'start',null); }
	document.body.ontouchmove = function(e) { if (e.touches.length==1) clickdown=1; lastclick=Date.now(); touchManager(e,'move',null); }
	document.body.ontouchend = function(e) { if (e.touches.length==0) clickdown=0; lastclick=Date.now(); touchManager(e,'end',null); }
	document.body.ontouchcancel = function(e) { if (e.touches.length==0) clickdown=0; lastclick=Date.now(); touchManager(e,'end',null); }
	divchatlogs=$("#divchatlogs");
	divtodobatalla=$("#divtodobatalla");
	divtapatodo=$("#divtapatodo_inn");
    $("div#divmanoductl").on("mouseenter", subiBajaMano)
        .on("mouseleave", subiBajaMano);
    $("#diveleccionescancel").on("click", function(ev) {
        $("#divelecciones")[0].onperform("_cancel");
        ev.stopPropagation();
        ev.preventDefault();
    });
    $("#diveleccionesdone").on("click", function(ev) {
        $("#divelecciones")[0].onperform("_complete");
        ev.stopPropagation();
        ev.preventDefault();
    });
    makeTabla();
    $(window).resize();
    // cambiarTodo(opc_chat_disp,opc_mano_disp,null);
    $(document).keypress(keypress);
    newConn();
    // makePoke([{img:11,hp:12,dano:9,nombre:"Charizard"},{img:5},{img:5},{img:5},{img:5},{img:5},{img:5},{img:5},{img:5},{img:5}],"b0op");
    // makePoke([{img:15,hp:10,dano:3,nombre:"Machamp"},{img:6},{img:7},{img:6},{img:6}],"b0du");
    // makePoke([{img:22,hp:10,dano:0,nombre:"Venusaur"},{img:4},{img:4},{img:4},{img:4}],"b1du");
    // makePoke([],"b2du");
    // makePoke([],"b3du");
    // makePoke([{img:28,hp:8,dano:7,nombre:"Electr0de"},{img:3},{img:3},{img:3},{img:4}],"b4du");
    // makePoke([],"b5du");
    // makePoke([],"staddu");
    // makePoke([{img:14,hp:7,dano:0,nombre:"Hitmonchan"},{img:6},{img:7}],"b1op");
    // makePoke([],"b2op");
    // makePoke([],"b3op");
    // makePoke([{img:55,hp:5,dano:1,nombre:"Doduo"},{img:7},{img:4},{img:4},{img:7}],"b4op");
    // makePoke([{img:77,hp:1,dano:0,nombre:"Clefairy Doll"},{img:2}],"b5op");
    // makeMano([14,66,2,1,77,99,102,7,8]);
    // makeMano([7,6]);
    // makeMano(manoop,false,true);
    // redrawMazo(60);
    // redrawMazo(30,true);
    // redrawDiscard([14,66,2,1,77,99,102,7,8]);
    // redrawDiscard([]);
    // redrawDiscard([14,66,2,1,77,14,66,2,1,77,14,66,2,1,77,14,66,2,1,77,45,35,25,100,78,23,44,33,11],true);
    // robarCartas(10,false,null);
    // robarCartas(10,true,null);
    // redrawStadium(45,true);
    // redrawStadium(54);
    // redrawSuporter(1,true);
    // redrawSuporter(4);
    // redrawPrices([1,2,0,-1,1,2,-1,3,0,-1,1,2]);
    // redrawPrices([0,0,0,null,0,0],true);
    // showElecciones("botones",["uno","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","dos","tres","tres","tres","tres"]);
    // $("#divmazodu").on("click",function(ev){robarCartas(1,false,function(){makeMano([rand(1,102)],true);});});
    // $("#divmazoop").on("click",function(ev){robarCartas(1,true,function(){manoop.push(0);makeMano(manoop,false,true);});});
    // $("#divelecciones").on("click",maxWindow);
	//taparTodo(-1);
});

function taparTodo(error,opc,inner) {
	if (error==-1) {
		divtapatodo.parent().fadeOut('slow');
	} else if (error=='error_loading') {
		divtapatodo.html(toLang.trans(error));
		divtapatodo.parent().fadeIn('slow');
	} else if (error=='error_wsconnecting') {
		divtapatodo.html(toLang.trans(error));
		divtapatodo.parent().fadeIn('slow');
	} else if (error=='error_wsconndie') {
		divtapatodo.html(toLang.trans(error));
		divtapatodo.parent().fadeIn('slow');
	} else if (error=='error_wsconnclose') {
		if (lasttapartodo!='error_wsconndie') {
			divtapatodo.html(toLang.trans(error));
			divtapatodo.parent().fadeIn('slow');
		}
	} else if (error=='error_login') {
		divtapatodo.html(toLang.trans(error));
		divtapatodo.parent().fadeIn('slow');
	}  else if (error=='error_joining') {
		divtapatodo.html(toLang.trans(error));
		divtapatodo.parent().fadeIn('slow');
	}
	lasttapartodo=error;
}

function maxWindow() {
    var tofu = divtodobatalla[0];
    if (tofu.webkitRequestFullScreen) tofu.webkitRequestFullScreen();
    else tofu.mozRequestFullScreen();

    isFullScreen = true;
    //document.getElementById('audio_m').play();
}
