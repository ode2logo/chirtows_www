function _objLang() {
    this.diccionario=[];
    this.add=function (key,txttrans) {
        this.diccionario.push({k:key,t:txttrans});
    }
    this.trans=function (key,vars) {
        var txt=this.diccionario.find(v=>v.k==key);
        if (!txt||!txt.t)
            txt=key;
        else
            txt=txt.t;
		if (vars&&vars.length) {
			for (var i=0;vars&&i<vars.length;i++) {
				txt=txt.replace("$"+i,vars[i]);
			}
		}
        return txt;
    }
    this.load=function (db_lang) {
        for (var i=0;db_lang&&i<db_lang.length;i++) {
            this.add(db_lang[i][0],db_lang[i][1]);
        }
    }
}
var toLang=new _objLang();
toLang.load([
['color_a',"Agua"] ,
['color_p',"Psíquico"] ,
['color_e',"Eléctrico"] ,
['color_h',"Hoja"] ,
['color_f',"Fuego"] ,
['color_l',"Lucha"] ,
['color_d',"Incoloro"] ,
['color_o',"Oscuro"] ,
['color_m',"Metal"] ,
["pos_b0","Activo"] ,
["pos_b1","BancaA"] ,
["pos_b2","BancaB"] ,
["pos_b3","BancaC"] ,
["pos_b4","BancaD"] ,
["pos_b5","BancaE"] ,
["pos_m","mazo"] ,
["pos_h","mano"] ,
["pos_d","descarte"] ,
["pos_p","punto"] ,
["pos_t","estadio"] ,
["pos_s","suporter"] ,
["moneda","Moneda"] ,
["manoAlDeck","$0 mezcla su mano en el mazo"] ,
["ini_mazo","$0 deja su mazo sobre la mesa"] ,
["rdy_player","$0 ha seleccionado su mazo"] ,
["rdy_pokes","$0 está listo para comenzar"] ,
["team_ganador","Final del encuentro"] ,
["set_turno","Inicia el turno $1 de $0"] ,
["player_fuera","$0 pierde por $1"] ,
["no_tiene_mazo","no tener cartas en mazo para robar"] ,
["baraja","$0 baraja su mazo"] ,
["robaCartas","$0 roba $1 carta(s)"] ,
["mueve:m:h","$0 deja a $1 en su mano"] ,
["mueve:d:h","$0 deja a $1 en su mano"] ,
["mueve:h:m","$0 deja a $1 en su mazo"] ,
["mueve:b:h","$0 deja a $1 de $2 en su mano"] ,
["bajaPoke:h:b","$0 baja de su mano a $1 en $2"] ,
["bajaPoke:d:b","$0 pone de su descarte a $1 en $2"] ,
["refreshPoke","$0 ($1) tiene nuevos atributos"] ,
["poneAdjunta:h:b","$0 adjunta $1 de su mano a $2"] ,
["poneAdjunta:w:b","$0 adjunta $1 de su descarte a $2"] ,
["evolucionaPoke:h:b","$0 evoluciona a $1 en $2 de su mano"] ,
["evolucionaPoke:m:b","$0 evoluciona a $1 en $2 de su mazo"] , //todavia no aparece
["devolucionaPoke:b","$0 devoluciona a $1 en $2"] , //puede dar error O.O
["robaPrice","$0 roba una carta de punto"] ,
["cambiaPoke","$0 es retirado"] ,
["cambiaPoke-1","$0 es puesto en Activo"] ,
["ataca","$0 usa el ataque $1"] ,
["pokepower","$0 usa el pokepower $1"] ,
["trainer","$0 usa el trainer $1"] ,
["cancelTrainer","$0 no pudo usar el trainer $1"] ,
["descarta:h","$0 descarta $1 de su mano"] ,
["descarta:b","$0 descarta $1 de $2"] ,
["pokeKO","($0) $1 ha sido nokeado"] ,
["contadores","$1 ha recibido $2HP de daño"] ,
["contadores-1","$1 ha recuperado $2HP"] ,
["error_loading","Algún objeto no pudo descargarse, recarga la página para volver a intentarlo."] ,
["error_wsconnecting","Conectando..."] ,
["error_wsconndie","No se pudo conectar al servidor.<br><button onclick='newConn()' class='recon'>Reintentar</button>"] ,
["error_wsconnclose","Has sido desconectado.<br><button onclick='newConn()' class='recon'>Reintentar</button>"] ,
["error_login","Esta sesión expiró o es inválida."] ,
["error_joining","La batalla a la que intentas acceder ya no existe."] ,
["start_battle","Iniciar Batalla"] ,
["no_have_basic","No tengo pokémon básico"] ,
["pokes_ready","Estoy listo"] ,
["pokes_not_ready","No aún"] ,

["eof","Fin"]
]);
